export const getUserFromLocalStorage = () => {
    const user = localStorage.getItem("user");
    return user ? JSON.parse(user): "";
};

export const getTokenFromLocalStorage = () => {
    const token = localStorage.getItem("token");
    return token ? token : "";
};