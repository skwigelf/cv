import {createMuiTheme} from "@material-ui/core/styles";
import {ruRU} from '@material-ui/core/locale';

const createdTheme = createMuiTheme({
    // Переопределяющее свойство
    overrides: {
        MuiFormControl: {
            root: {
                width: '100%',
                background: "#ffffff",
                borderWidth: '2px',
                borderColor: '#d4dae7',
                textDecoration: 'none',
                padding: '10px 0 10px 16px',
                borderRadius: '15px',
                borderStyle: 'solid',
                boxSizing: 'border-box',
                height: '42px',

            }
        },
        MuiPickersToolbar: {
            toolbar: {
                backgroundColor: "#9636d5",
            }
        },
        MuiTypography: {
            colorPrimary: {
                color: "#9636d5",
            },
        },
        MuiInput: {
            underline: {
                '&:before': {
                    display: 'none',
                },
                '&:after': {
                    display: 'none',
                }
            },
            formControl: {
                marginTop: '0px !important'
            }
        },
        MuiInputBase: {
            input: {
                padding: '0'
            }
        }
    }
}, ruRU);

export default createdTheme;