import MuiTextField from '@material-ui/core/TextField';
import { fieldToTextField, TextFieldProps } from 'formik-material-ui';
import * as React from "react";
import {DatePicker} from "@material-ui/pickers";
import {withStyles} from "@material-ui/core";
import styles from "./PickerStyles";

const YearPicker = ({ field, form,  ...other }) => {
    //const currentError = form.errors[field.name];
    const {classes} = other;
    return (
        <DatePicker
            views={["year"]}
            name={field.name}
            value={field.value}
            // if you are using custom validation schema you probably want to pass `true` as third argument
            onChange={date => {
                console.log(date);
                form.setFieldValue(field.name, date, false)}
            }
            {...other}
        />
    );
};

export default withStyles(styles)(YearPicker);