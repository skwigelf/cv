export const getTemplates = (token) => {
    return fetch('http://localhost:8075/api/rest/template', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        return response.json()
    })
};