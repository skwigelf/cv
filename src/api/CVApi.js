export const chooseCVTemplate = (id, values, token) => {
    return fetch('http://localhost:8080/api/rest/cv/' + id + '/choose-template', {
        method: 'POST',
        body: values,
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        return response.text();
    })
};

export const updateDataSourceType = (id, values, token) => {
    return fetch('http://localhost:8080/api/rest/cv/' + id + '/data-source-type', {
        method: 'PUT',
        body: values,
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        return response.text();
    })
};

export const updateCVData = (id, values, token) => {
    return fetch('http://localhost:8080/api/rest/cv/' + id + '/update-data', {
        method: 'PUT',
        body: JSON.stringify(values),
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
    })
};

export const updateCVFromHH = (id, data, token) => {
    const formData = new FormData();

    formData.append('pdf', data);
    return fetch('http://localhost:8080/api/rest/cv/' + id + '/hh-data', {
        method: 'POST',
        body: formData,
        headers: {
            'Authorization': 'Bearer ' + token,
        },
    })
};

export const submitCV = (id, values, token) => {
    return fetch('http://localhost:8080/api/rest/cv/' + id + '/submit', {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        return response.text();
    })
};

export const getCVs = (token) => {
    return fetch('http://localhost:8080/api/rest/cv/user', {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        return response.json();
    })
};

export const getCVDocument = (id, token) => {
    return fetch('http://localhost:8080/api/rest/cv/' + id + '/document', {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token,
            //'Content-Type': 'application/json'
        },
    })
        .then((response) => {
            return response.blob()
        })
        .then((blob) => {
            //Страшнейшая костылина с https://medium.com/yellowcode/download-api-files-with-react-fetch-393e4dae0d9e
            //не верю что нельзя по человечески, но пока так
            const url = window.URL.createObjectURL(new Blob([blob]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', "резюме.pdf");
            document.body.appendChild(link);
            link.click();
            link.parentNode.removeChild(link);
        })
        .catch((error) => {
            console.log(error);
        });
};

export const getCV = (id, token) => {
    return fetch('http://localhost:8080/api/rest/cv/' + id, {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        if (response.status === 200) {
            return response.json();
        }
        throw Error();
    })
};

export const createCV = (token) => {
    return fetch('http://localhost:8080/api/rest/cv', {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        if (response.status === 200) {
            return response.text();
        }
        throw Error();
    })
};

export const changeCVTemplate = (id, token) => {
    return fetch('http://localhost:8080/api/rest/cv/' + id + '/change-template', {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        return response.text();
    })
};