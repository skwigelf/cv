export const createPdf = (values, templateId) => {
    return fetch('http://localhost:8075/api/rest/docs/' + templateId, {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
            'Content-Type': 'application/json'
        },
    })
        .then((response) => {
            return response.blob()
        })
        .then((blob) => {
            const fileURL = URL.createObjectURL(new Blob([blob], {type: 'application/pdf'}));
            window.open(fileURL);
            //Страшнейшая костылина с https://medium.com/yellowcode/download-api-files-with-react-fetch-393e4dae0d9e
            //не верю что нельзя по человечески, но пока так
            /*const url = window.URL.createObjectURL(new Blob([blob]));
            const link = document.createElement('a');
            link.setAttribute("target", "_blank");
            link.href = url;
            link.setAttribute('download', "резюме.pdf");
            document.body.appendChild(link);
            link.click();
            link.parentNode.removeChild(link);*/
        })
        .catch((error) => {
            console.log(error);
        });

};

export const createPdfNoAuth = (values, templateId) => {
    return fetch('http://localhost:8075/api/rest/docs/' + templateId, {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
            'Content-Type': 'application/json'
        },
    })
        .then((response) => {
            return response.blob()
        })
        .then((blob) => {

            //Страшнейшая костылина с https://medium.com/yellowcode/download-api-files-with-react-fetch-393e4dae0d9e
            //не верю что нельзя по человечески, но пока так
            const url = window.URL.createObjectURL(new Blob([blob]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', "резюме.pdf");
            document.body.appendChild(link);
            link.click();
            link.parentNode.removeChild(link);
        })
        .catch((error) => {
            console.log(error);
        });
};