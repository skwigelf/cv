export const loginApi = (values) => {
    return fetch('http://localhost:8081/api/login', {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        if (response.status === 200) {
            return response.json()
        }
        throw Error();
    })
};

export const registerApi = (values) => {
    return fetch('http://localhost:8080/api/users', {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        if (response.status === 200) {
            return response.json()
        }
        throw Error();
    })
};

export const userInfoUpdateApi = (token, values) => {
    return fetch('http://localhost:8080/api/users/user-data', {
        method: 'PUT',
        body: JSON.stringify(values),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
    }).then((response) => {
        if (response.status === 200) {
            return response.json()
        }
        throw Error();
    })
};

export const getUserInfoApi = (token) => {
    return fetch('http://localhost:8080/api/users/user-data', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
    }).then((response) => {
        if (response.status === 200) {
            return response.json()
        }
        throw Error();
    })
};