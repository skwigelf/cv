import React, {createContext, useState} from 'react';
import './App.css';
import Header from "./components/common/Header/Header";
import {BrowserRouter} from "react-router-dom";
import {Route, Switch} from "react-router";
import MainPage from "./components/screens/MainPage/MainPage";
import CVContainer from "./components/screens/CVCreator/CVContainer";
import NotFound from "./components/screens/ErrorPage/NotFound";
import SecuredRoute from "./components/common/SecuredRoute";
import LoginModal from "./components/screens/LoginModal/LoginModal";
import UserAccount from "./components/screens/UserAccount/UserAccount";
import Login from "./components/screens/Login/Login";
import CVContainerNoAuth from "./components/screens/CVCreator/CVContainerNoAuth";
import {MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';
import Footer from "./components/common/Footer/Footer";
import { ThemeProvider } from "@material-ui/core/styles";
import createdTheme from "./util/CreatedTheme";

function App() {

    return (
        <>
            <ThemeProvider theme={createdTheme}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <BrowserRouter>

                        <div style={{flex: '1 0 auto'}}>
                            <Header/>
                            <main>
                                <Switch>
                                    <Route exact path="/" component={MainPage}/>
                                    <Route exact path="/login" component={Login}/>
                                    <Route exact path="/cv/new" component={CVContainerNoAuth}/>
                                    <SecuredRoute exact path="/cv/:id" component={CVContainer} authorities={["USER"]}/>
                                    <SecuredRoute exact path="/cv/:id" component={CVContainer} authorities={["USER"]}/>
                                    <SecuredRoute exact path="/usvers" component={UserAccount} authorities={["USER"]}/>
                                    <Route exact path="/**" component={NotFound}/>
                                </Switch>
                            </main>
                        </div>
                        <Footer/>
                    </BrowserRouter>
                </MuiPickersUtilsProvider>
            </ThemeProvider>
        </>
    );
}

export default App;
