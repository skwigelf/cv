import React from 'react';
import '../DefaultStyles.css';
import {chooseCVTemplate, updateCVData} from "../../../api/CVApi";
import {getTokenFromLocalStorage} from "../../../util/functions";
import {withStyles} from "@material-ui/core";
import styles from "./TemplateItemStyles";

const TemplateItem = (props) => {

    const {setStatus, setTemplateId, cvId, isActive, activeTemplate, setActiveTemplate, cv, templateCount, template, userData, classes} = props;
    const token = getTokenFromLocalStorage();
    const {title, description, previewImage, id} = template;
    const handleClick = () => {
        if (token) {
            chooseCVTemplate(cvId, id, token).then((result) => {

                if (userData && cv && !cv.data) {
                    setStatus(result.status);
                    /*updateCVData(cvId, userData, token)
                        .then(() => {
                            setStatus(result.status);
                        })
                        .catch(() => {
                        })*/
                } else {
                    setStatus(result.status);
                }
            });
        } else {
            setTemplateId(id);
            setStatus("FILL_DATA");
        }
    };
    return (
        <div className={isActive ? classes.active : classes.unactive}>

            <div className={classes.templateItemContainer}>
                <div className={classes.templateTop}>
                    <div className={classes.arrow} onClick={() => {
                        if (activeTemplate > 0) {
                            setActiveTemplate(activeTemplate - 1)
                        }
                    }
                    }>
                        <img src="/left_arrow.svg" alt=""/>
                    </div>
                    <div className={classes.templateItemImageContainer}>
                        <img src={`data:image/jpg;base64,${previewImage}`} alt=""/>
                    </div>
                    <div className={classes.arrow} onClick={() => {
                        console.log(templateCount)
                        if (activeTemplate < templateCount - 1) {
                            setActiveTemplate(activeTemplate + 1)
                        }
                    }
                    }>
                        <img src="/right_arrow.svg" alt=""/>
                    </div>
                </div>

                <div className={classes.textWrapper}>
                    <div className={classes.templateItemTextContainer}>
                        <div onClick={handleClick} className={"default-link"}><h3>{title}</h3></div>
                        <div className={classes.templateItemLine}/>
                        <p className={classes.description}>{description}</p>
                    </div>
                </div>

            </div>

        </div>
    );
};

export default withStyles(styles)(TemplateItem);
