const styles = (theme) => ({
    templateItemContainer: {
        width: '30%',
        marginTop: '50px',
        display: 'flex',
    },
    templateItemImageContainer: {
        borderRadius: '4px',
        border: '1px solid',
        padding: '15px',
        marginRight: '15px',
        '& img': {
            width: '140px',
            height: '200px',
            borderRadius: '15px',
            marginRight: '30px',
        }
    },
    templateItemTextContainer: {},
    templateTop: {
        display: 'flex',
    },
    templateItemLine: {
        width: '190px',
        height: '2px',
        paddingLeft: '3px',
        paddingRight: '3px',
        backgroundColor: '#9636d5',
    },
    arrow: {
        display: 'none',

    },
    active: {},
    unactive: {},
    textWrapper: {},
    '@media (max-width: 768px)': {
        arrow: {
            display: 'flex',
            marginLeft: '15px',
            marginRight: '15px',
            cursor: 'pointer',
        },
        templateItemContainer: {
            width: '100%',
            marginTop: '50px',
            display: 'block',

        },
        templateTop: {
            display: 'flex',
            width: '100%',
            justifyContent: 'center',
        },
        active: {
            width: '100%',
        },
        unactive: {
            display: 'none',
        },
        templateItemImageContainer: {
            marginRight: '15px',
        },
        templateItemTextContainer: {},
        textWrapper: {
            display: 'flex',
            justifyContent: 'center',
        },
    },
});

export default styles;