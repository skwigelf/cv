import React from 'react';
import {withStyles} from "@material-ui/core";
import styles from "./FileUploaderStyles";

const FileUploader = props => {

    const {classes, text} = props;
    const hiddenFileInput = React.useRef(null);

    const handleClick = event => {
        hiddenFileInput.current.click();
    };
    const handleChange = event => {
        const fileUploaded = event.target.files[0];
        props.handleFile(fileUploaded);
    };
    return (
        <>
            <div onClick={handleClick} className={classes.customFileUpload} >
                {text}
            </div>
            <input type="file"
                   className={classes.fileInput}
                   ref={hiddenFileInput}
                   onChange={handleChange}
                   style={{display:'none'}}
            />
        </>
    );
};
export default withStyles(styles)(FileUploader);