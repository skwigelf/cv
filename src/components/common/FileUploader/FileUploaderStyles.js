const styles = (theme) => ({
    customFileUpload: {
        display: "inline-block",
        padding: "6px 12px",
        cursor: "pointer",
        color: '#883eb0',
    },
    fileInput: {
        display: "none"
    },
});

export default styles;