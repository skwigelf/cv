const styles = (theme) => ({
    '@global html': {
        height: '100%',
    },
    '@global body': {
        height: '100%',
    },
    '@global footer': {
        background: '#ffffff',
        paddingTop: '50px',
        paddingBottom: '50px',
        flex: '0 0 auto',
        marginTop: '50px',
    },
    footerColumnContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    footerColumn: {
        color: '#883eb0',
        paddingLeft: '20px',
        paddingRight: '20px',
    },
    footerRow: {
        marginTop: '20px',
        marginBottom: '20px',
    },

    '@media (max-width: 768px)': {
        button: {
            width: '200px',
        },
        menuButton: {
            visibility: 'visible',
        },
        headerContainer: {
            flexDirection: 'row-reverse',

        },
        flexMenuWrapper: {
            visibility: 'hidden',
        },
        headerButton: {
            height: '31px',
            width: '100%',
            paddingLeft: '15px',
            paddingRight: '15px',
            textAlign: 'center',
            lineHeight: '31px',
            boxSizing: 'border-box',
            cursor: 'pointer',
            margin: '0',
        },
        signUpButton: {
            backgroundColor: '#ffffff',
            color: '#000000',
        },
        registerButton: {
            backgroundColor: '#ffffff',
            color: '#000000',
        },
        leftNavMenu: {
            visibility: 'visible',
            position: 'absolute',
            top: '0',
            left: '0',
            bottom: '0',
            zIndex: '293',
            display: 'block',
            width: '50%',
            maxWidth: '100%',
            marginTop: '0px',
            paddingRight: '0px',
            alignItems: 'stretch',
            backgroundColor: '#ffffff',
            paddingTop: '100px',
            transform: 'translateX(-100%)',
            transition: 'all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1)',
            '& a:hover': {
                backgroundColor: '#883eb0',
                color: '#ffffff',
            }
        },

    },
});

export default styles;