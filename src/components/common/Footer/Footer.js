import React, {useEffect, useRef, useState} from 'react';
import styles from "./FooterStyles.js";
import {Link, useHistory} from "react-router-dom";
import {withStyles} from "@material-ui/core";

const Footer = (props) => {

    const {classes} = props;
    const node = useRef();
    const history = useHistory();

    return (
        <>
            <footer>
                <div className={classes.footerColumnContainer}>
                    <div className={classes.footerColumn}>
                        <Link to="/about" className={classes.footerRow}>
                            О нас
                        </Link>
                        <div className={classes.footerRow}>
                            info@cveria.com
                        </div>
                    </div>
                    <div className={classes.footerColumn}>
                        <div className={classes.footerRow}>
                            Политика конфиденциальности
                        </div>
                        <div className={classes.footerRow}>
                            Использование личных данных
                        </div>
                    </div>
                    <div className={classes.footerColumn}>
                        <div className={classes.footerRow}>
                            Instagram
                        </div>
                        <div className={classes.footerRow}>
                            VK
                        </div>
                    </div>
                </div>
            </footer>
        </>
    );
};


export default withStyles(styles)(Footer);
