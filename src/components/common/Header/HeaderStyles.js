const styles = (theme) => ({
    '@global header': {
        fontFamily: 'Gothic Century, sans-serif',
        background: '#ffffff',
        paddingTop: '20px',
        paddingBottom: '20px',
        textAlign: 'left',
    },
    headerContainer: {
        visibility: 'visible',
        margin: 'auto',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        '& .header-container': {
            color: '#edf4f6',
        },
    },
    headerMenuWrapper: {},
    flexMenuWrapper: {
        display: 'flex',
        justifyContent: 'space-between',
    },

    leftNavMenu: {
        display: 'none',
    },
    menuActive: {
        transform: 'translateX(0%)',
    },

    headerWrapper: {
        width: '1250px',
        boxSizing: 'border-box',
        paddingLeft: '25px',
        paddingRight: '25px',
        margin: 'auto',
    },


    headerButton: {
        height: '31px',
        width: '110px',
        textAlign: 'center',
        lineHeight: '31px',
        borderRadius: '5px',
        marginRight: '10px',
        marginLeft: '10px',
        display: 'block',
        cursor: 'pointer',
    },
    signUpButton: {
        backgroundColor: '#883eb0',
        color: '#ffffff',
    },
    otherScreen: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        opacity: '0.8',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        top: '0',
        left: '0',
        zIndex: '100',
    },
    registerButton: {
        backgroundColor: '#edf4f6',
        color: '#000000',
    },
    menuContainer: {
        display: 'flex',
        justifyContent: 'spaceBetween',
        alignSelf: 'center',
    },
    menuButton: {
        visibility: 'hidden',
        width: '50px',
        height: '50px',
        zIndex: '789',
        cursor: 'pointer',
        '& img': {
            width: '30px',
            height: '30px',
        }
    },
    '@media (max-width: 1250px)': {
        headerWrapper: {

            width: '100%'
        },
    },
    '@media (max-width: 768px)': {
        button: {
            width: '200px',
        },
        menuButton: {
            visibility: 'visible',
        },
        headerContainer: {
            flexDirection: 'row-reverse',

        },
        flexMenuWrapper: {
            visibility: 'hidden',
        },
        headerButton: {
            height: '31px',
            width: '100%',
            paddingLeft: '15px',
            paddingRight: '15px',
            textAlign: 'center',
            lineHeight: '31px',
            boxSizing: 'border-box',
            cursor: 'pointer',
            margin: '0',
        },
        signUpButton: {
            backgroundColor: '#ffffff',
            color: '#000000',
        },
        registerButton: {
            backgroundColor: '#ffffff',
            color: '#000000',
        },
        leftNavMenu: {
            visibility: 'visible',
            position: 'absolute',
            top: '0',
            left: '0',
            bottom: '0',
            zIndex: '293',
            display: 'block',
            width: '50%',
            maxWidth: '100%',
            marginTop: '0px',
            paddingRight: '0px',
            alignItems: 'stretch',
            backgroundColor: '#ffffff',
            paddingTop: '100px',
            transform: 'translateX(-100%)',
            transition: 'all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1)',
            '& a:hover': {
                backgroundColor: '#883eb0',
                color: '#ffffff',
            }
        },

    },
});

export default styles;