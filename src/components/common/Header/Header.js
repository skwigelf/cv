import React, {useEffect, useRef, useState} from 'react';
import styles from "./HeaderStyles";
import logo from "../../../cverialogo.png";
//import './Header.css'
import {Link, useHistory} from "react-router-dom";
import LoginModal from "../../screens/LoginModal/LoginModal";
import {withStyles} from "@material-ui/core";
import RegisterModal from "../../screens/RegisterModal/RegisterModal";
import {getTokenFromLocalStorage, getUserFromLocalStorage} from "../../../util/functions";
import useOnClickOutside from "../../../util/hooks/UseOnClickOutside";
import {createCV} from "../../../api/CVApi";

const Header = (props) => {

    const {classes} = props;
    const node = useRef();
    const history = useHistory();
    const [loginModal, setLoginModal] = useState(false);
    const [registerModal, setRegisterModal] = useState(false);
    const [isMenuOpened, setIsMenuOpened] = useState(false);
    const [user, setUser] = useState();
    const [token, setToken] = useState();
    useOnClickOutside(node, () => {
        // Only if menu is open
        if (isMenuOpened) {
            setIsMenuOpened(!isMenuOpened);
        }
    });

    useEffect(() => {
        setUser(getUserFromLocalStorage());
        setToken(getTokenFromLocalStorage());
    }, [loginModal, registerModal]);

    const handleLoginModal = () => {
        setLoginModal(!loginModal);
        setRegisterModal(false);
    };
    const handleRegisterModal = () => {
        setRegisterModal(!registerModal);
        setLoginModal(false);
    };
    const handleLogOut = () => {
        localStorage.clear();
        setUser("");
        setToken("");
    };
    const handleMenuClick = () => {
        setIsMenuOpened(!isMenuOpened);
        console.log(classes);
    };

    const handleNewResume = () => {
        createCV(token).then((result) => {
            history.push("/cv/" + result);
        })
            .catch((response) => {
                history.push("/cv/0");
            });

    };

    return (
        <header ref={node}>
            <div className={classes.headerWrapper}>
                <div className={classes.headerContainer}>
                    <Link to={"/"}>
                        <img src={logo} className={classes.logoCv} alt="logo"/>
                    </Link>

                    <div className={classes.menuContainer}>
                        <div className={classes.menuButton} onClick={handleMenuClick}>
                            <img src="/camry.svg" alt=""/>
                        </div>
                        <div className={classes.headerMenuWrapper}>
                            <div className={classes.flexMenuWrapper}>
                                {!user
                                    ? <>
                                        <a className={classes.headerButton + ' ' + classes.signUpButton}
                                           onClick={handleLoginModal}>
                                            <span>Войти</span>
                                        </a>
                                        <a className={classes.headerButton + ' ' + classes.registerButton}
                                           onClick={handleRegisterModal}>
                                            <span>Регистрация</span>
                                        </a>
                                    </>
                                    : <>

                                        <Link className={classes.headerButton + ' ' + classes.signUpButton}
                                              to={"/usvers"}>
                                            <span>Аккаунт</span>
                                        </Link>

                                        <a className={classes.headerButton + ' ' + classes.registerButton}
                                           onClick={handleLogOut}>
                                            <span>Выйти</span>
                                        </a>
                                    </>
                                }
                            </div>
                        </div>

                    </div>
                </div>
                {isMenuOpened ? <div onClick={() => setIsMenuOpened(!isMenuOpened)} className={classes.otherScreen}/> : ""}
                <div style={isMenuOpened ? {transform: 'translateX(0%)'} : {}} className={classes.leftNavMenu}>
                    <nav>
                        {!user
                            ? <>
                                <Link className={classes.headerButton + ' ' + classes.signUpButton} to={"/login"}>
                                    <span>Войти</span>
                                </Link>
                                <Link className={classes.headerButton + ' ' + classes.registerButton} to={"/register"}>
                                    <span>Регистрация</span>
                                </Link>
                            </>
                            : <>

                                <Link className={classes.headerButton + ' ' + classes.signUpButton} to={"/usvers"}>
                                    <span>Личный кабинет</span>
                                </Link>

                                <Link className={classes.headerButton + ' ' + classes.registerButton}
                                      onClick={handleLogOut}>
                                    <span>Выйти</span>
                                </Link>
                            </>
                        }
                        <Link className={classes.headerButton + ' ' + classes.signUpButton} onClick={handleNewResume}>
                            <span>Create new</span>
                        </Link>
                        <Link className={classes.headerButton + ' ' + classes.signUpButton} to={"/usvers"}>
                            <span>Contact</span>
                        </Link>
                    </nav>
                </div>
            </div>
            {loginModal ? <><LoginModal handleModal={handleLoginModal}/><div onClick={() => setLoginModal(!loginModal)} className={classes.otherScreen}/></> : ""}
            {registerModal ? <><RegisterModal handleModal={handleRegisterModal}/><div onClick={() => setRegisterModal(!registerModal)} className={classes.otherScreen}/></> : ""}
        </header>
    );
};


export default withStyles(styles)(Header);
