const styles = (theme) => ({
    modalContainer: {
        width: '500px',
        margin: 'auto',
        background: '#d4dae7',
        position: 'fixed',
        right: '0',
        left: '0',
        zIndex: '501',
        borderRadius: '10px',
    },
    modalContainerWhite: {
        width: '500px',
        margin: 'auto',
        background: '#ffffff',
        position: 'fixed',
        right: '0',
        left: '0',
        zIndex: '501',
        borderRadius: '10px',
    },
    modalExitContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    exitButton: {
        cursor: 'pointer',
        width: '15px',
        height: '15px',
        background: '#883eb0'
    },
    modalHeaderContainer: {
        textAlign: 'center',
        color: '#883eb0',
        '& h1': {
            fontSize: '50px',
        },
    },
});

export default styles;