import React from 'react';
import '../DefaultStyles.css';
import {withStyles} from "@material-ui/core";
import styles from "./ModalStyles";

const Modal = (props) => {
    const {content, handleModal, whiteStyle, classes} = props;

    return (
        <div className={whiteStyle ? classes.modalContainerWhite : classes.modalContainer}>
            <div className={classes.modalExitContainer}>
                <div/>
                <div className={classes.exitButton} onClick={handleModal}>

                </div>
            </div>
            <div className={classes.modalHeaderContainer}>
                <h1>CVeria</h1>
            </div>
            {content}
        </div>
    );
};

export default withStyles(styles)(Modal);
