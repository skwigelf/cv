import {Field} from "formik";
import React from "react";


const LabelTextArea = (props) => {

    const {id, label, placeholder, labelClassName, inputClassName, containerClassName} = props;
    return (
        <div className={containerClassName}>
            <label htmlFor={id} className={labelClassName}>{label}</label>
            <Field id={id} label={label}  {...props}
                   component={() => <textarea className={inputClassName} placeholder={placeholder}/>}/>
        </div>
    );
};

export default LabelTextArea;