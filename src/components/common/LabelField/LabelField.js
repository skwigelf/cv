import {Field} from "formik";
import React from "react";


const LabelField = (props) => {

    const {id, label, labelClassName, inputClassName, containerClassName} = props;
    return (
        <div className={containerClassName}>
            <label htmlFor={id} className={labelClassName}>{label}</label>
            <Field id={id} label={label} className={inputClassName} {...props}/>
        </div>
    );
};

export default LabelField;