import React from 'react';
import '../../common/DefaultStyles.css';
import './MainPage.css';
import {useHistory} from "react-router-dom";
import {createCV} from "../../../api/CVApi";
import {getTokenFromLocalStorage} from "../../../util/functions";

const MainPage = (props) => {

    const history = useHistory();
    const token = getTokenFromLocalStorage();
    const handleClick = () => {
        if (token) {
            createCV(token).then((result) => {
                history.push("/cv/" + result);
            })
                .catch((response) => {
                    history.push("/cv/0");
                });
        }
        else {
            history.push("/cv/new");
        }
    };

    return (
        <div className={"main-page-container"}>
            <div className={"main-page-header-container"}>
                <h1>Резюме. <span className={"easy-style"}>Идеально.</span> Легко.</h1>
            </div>
            <div onClick={handleClick} className={"main-page-button-container"}>
                Создать
            </div>
            <div className={"main-screen"}>
                <div className={"left-logo-text-containter"}>
                    <h2>CVeria</h2>
                </div>
                <div></div>
                <div className={"why-we-text-containter"}>
                    <h2>Почему мы?</h2>
                    <ul>
                        <li>- Большое количество бесплатных шаблонов</li>
                        <li>- Интеграция с рекрутинговыми сервисами</li>
                        <li>- Легко использовать</li>
                        <li>- Помощь в составлении резюме</li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default MainPage;
