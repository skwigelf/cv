import React, {useState} from 'react';
import '../../common/DefaultStyles.css';
import Modal from "../../common/Modal/Modal";
import {Field, Form, Formik} from "formik";
import {loginApi} from "../../../api/UserApi";
import jwt_decode from "jwt-decode";
import {Link} from "react-router-dom";
import {withStyles} from "@material-ui/core";
import styles from "./NewCVModalStyles";
import {updateCVData, updateCVFromHH, updateDataSourceType} from "../../../api/CVApi";
import FileUploader from "../../common/FileUploader/FileUploader";

const NewCVModal = (props) => {
    const {handleModal, cv, token, userData, handleButton, classes} = props;

    const [selectedFile, setSelectedFile] = useState();
    const [isFilePicked, setIsFilePicked] = useState(false);

    const handleFile = (file) => {
        setSelectedFile(file);
    };

    return (
        <Modal whiteStyle={true} handleModal={handleModal} content={
            <div className={classes.newCVModalContainer}>
                <div className={classes.cvChooserContainer}>
                    <div className={classes.text}>
                        Вот тут будет импорт с хх
                    </div>

                        <FileUploader text={"+ Добавить резюме"} handleFile={handleFile}/>
                    {selectedFile ?  <div className={classes.uploadedFileName}>{selectedFile.name}</div> : ""}


                    <div className={classes.cvButton}
                         onClick={() => {
                             updateCVFromHH(cv.id, selectedFile, token)
                                 .then(r => {
                                     console.log(r);
                                     if (r.status === 200) {
                                         handleButton();
                                     }
                                 })
                                 .catch(ff => {

                                 });

                         }}> Начать
                    </div>
                </div>
                <div className={classes.purpleLine}/>

                <div className={classes.cvChooserContainer}>
                    <div>
                        Вы можете импортировать ваши данные с личного кабинета
                    </div>

                    <div className={classes.cvButton} onClick={() => {

                        updateDataSourceType(cv.id, JSON.stringify("ACCOUNT"), token)
                            .then(() => {
                                updateCVData(cv.id, userData ? userData : "", token)
                                    .then(() => {
                                        handleButton();
                                    });
                            });
                    }}> Начать
                    </div>
                </div>
                <div className={classes.purpleLine}/>

                <div className={classes.cvChooserContainer}>
                    <div>
                        Пустое резюме
                    </div>

                    <div className={classes.cvButton}
                         onClick={() => {
                             updateDataSourceType(cv.id, JSON.stringify("NO_DATA"), token)
                                 .then(() => {
                                     handleButton();
                                 })

                         }}> Начать
                    </div>
                </div>
            </div>
        }/>
    );
};


export default withStyles(styles)(NewCVModal);
