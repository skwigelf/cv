const styles = (theme) => ({
    cvChooserContainer: {
        paddingTop: '10px',
        background: '#ffffff',
        width: '80%',
        margin: 'auto',
        paddingBottom: '50px',
        textAlign: 'center',
    },
    uploadedFileName: {
      paddingBottom: "10px",
    },
    purpleLine: {
        background: '#883eb0',
        height: '2px',
        width: '100%'
    },
    newCVModalContainer: {
        paddingLeft: '25px',
        paddingRight: '25px',
    },
    cvButton: {
        backgroundColor: '#883eb0',
        textAlign: 'center',
        //paddingTop: '10px',
        //paddingBottom: '10px',
        height: '34px',
        lineHeight: '34px',
        width: '110px',
        margin: 'auto',
        display: 'block',
        color: '#ffffff',
        fontSize: '15px',
        textDecoration: 'none',
        border: 'none',
        cursor: 'pointer',
        borderRadius: '5px',
    },
});

export default styles;