const styles = (theme) => ({
    formContainer: {
        paddingTop: '10px',
        width: '80%',
        margin: 'auto',
        paddingBottom: '50px',
    },
    loginButton: {
        backgroundColor: '#883eb0',
        textAlign: 'center',
        paddingTop: '10px',
        paddingBottom: '10px',
        width: '150px',
        margin: 'auto',
        display: 'block',
        color: '#ffffff',
        fontSize: '15px',
        textDecoration: 'none',
        border: 'none',
        cursor: 'pointer',
    },
    loginError: {
        textAlign: 'center',
        color: '#dd3b61',
        marginTop: '15px',
    }
});

export default styles;