import React, {useState} from 'react';
import '../../common/DefaultStyles.css';
import Modal from "../../common/Modal/Modal";
import {Field, Form, Formik} from "formik";
import {loginApi} from "../../../api/UserApi";
import jwt_decode from "jwt-decode";
import {Link} from "react-router-dom";
import {withStyles} from "@material-ui/core";
import styles from "./LoginModalStyles";

const LoginModal = (props) => {
    const {handleModal, classes} = props;
    const [isLoginError, setIsLoginError] = useState(false);
    return (
        <Modal handleModal={handleModal} content={
            <div className={classes.loginModalContainer}>
                <div className={classes.formContainer}>
                    <Formik initialValues={{
                        username: '',
                        password: ''
                    }} onSubmit={(values) => {
                        loginApi(values).then((result) => {
                            const token = result.token;
                            const user = jwt_decode(token).iss;
                            localStorage.setItem("token", token);
                            localStorage.setItem("user", user);
                            handleModal();

                            //setLoginError(false);
                        })
                            .catch((result) => {
                              //  setLoginError(true);
                                setIsLoginError(true);
                            });
                    }}>
                        <Form onChange={() => setIsLoginError(false)}>
                            <Field id="username" type={"login"} name={`username`}
                                   className={"input-user-default"}
                                   placeholder={"Логин"}/>
                            <Field id="password" type={"password"} name={`password`}
                                   className={"input-user-default"}
                                   placeholder={"Пароль"}/>
                            <Link to={"/register"}>
                                Регистрация
                            </Link>
                            <button type={"submit"} className={classes.loginButton}>
                                Войти
                            </button>
                            {isLoginError ? <div className={classes.loginError}>Неверный логин или пароль</div> : ""}
                        </Form>
                    </Formik>
                </div>

            </div>

        }/>
    );
};


export default  withStyles(styles)(LoginModal);
