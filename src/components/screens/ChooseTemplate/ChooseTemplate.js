import React, {useEffect, useState} from 'react';
import TemplateItem from "../../common/TemplateMenuItem/TemplateItem";
import styles from "./ChooseTemplateStyles";
import '../../common/DefaultStyles.css';
import {getTemplates} from "../../../api/TemplateApi";
import {getUserInfoApi} from "../../../api/UserApi";
import withStyles from "@material-ui/core/styles/withStyles";
import NewCVModal from "../NewCVModal/NewCVModal";

const ChooseTemplate = (props) => {

    const {classes, setStatus, token, cv, setTemplateId, cvId} = props;
    const [templates, setTemplates] = useState([]);
    const [userData, setUserData] = useState("");
    const [activeTemplate, setActiveTemplate] = useState(0);
    const [isLoaded, setIsLoaded] = useState(false);
    const [dataSourceType, setDataSourceType] = useState("");
    const [isError, setIsError] = useState(false);
    const handleButton = () => {
        setDataSourceType(true);
    };
    console.log(cv);
    console.log(cv.dataSourceType);
    useEffect(() => {
        getTemplates().then((result) => {
            setTemplates(result);
            setIsLoaded(true);
        })
            .catch(() => {
                setIsLoaded(true);
                setIsError(true);
            });
        if (cv && cv.dataSourceType) {
            setDataSourceType(true);
        }
        if (token) {
            getUserInfoApi(token)
                .then((result) => {
                    setUserData(result);
                })
                .catch(() => {

                })
        }
    }, []);


    return (
        <div className={"screen-container"}>
            {!dataSourceType
                ? <><NewCVModal cv={cv} token={token} userData={userData} handleButton={handleButton}/><div className={classes.otherScreen}/></>
                : ""
            }
            <div className={classes.templateItemContainer}>
                <h1>Шаг 1. Выбери свой стиль</h1>
            </div>

            <div className={classes.chooseTemplateItemContainer}>

                {isLoaded ?

                    templates.map((key, val) => {
                        return (<TemplateItem cvId={cvId}
                                              cv={cv}
                                              isActive={val === activeTemplate}
                                              templateCount={templates.length}
                                              activeTemplate={activeTemplate}
                                              setActiveTemplate={setActiveTemplate}
                                              setStatus={setStatus}
                                              setTemplateId={setTemplateId}
                                              template={key}
                                              userData={userData}/>
                        )
                    })

                    : <div className={classes.previewContainer}><img src="/logo.gif" alt=""/></div>
                }
            </div>
        </div>
    );
};


export default withStyles(styles)(ChooseTemplate);
