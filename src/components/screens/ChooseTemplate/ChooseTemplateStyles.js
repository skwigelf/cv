const styles = (theme) => ({
    templateItemContainer: {
        width: '50%',
        marginTop: '50px',
        display: 'flex',
    },
    chooseTemplateItemContainer: {
        width: '1250px',
        display: 'flex',
        alignContent: 'flex-start',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
    },
    arrow: {
        display: 'none',

    },
    otherScreen: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        opacity: '0.8',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        top: '0',
        left: '0',
        zIndex: '100',
    },
    previewContainer: {

        margin: 'auto',
        '& img': {
            width: '250px',
            height: '250px',
        }
    },
    '@media (max-width: 1250px)': {
        chooseTemplateItemContainer: {
            width: '100%',
            paddingLeft: '15px',
            paddingRight: '15px',
            boxSizing: 'border-box'
        },
        templateItemContainer: {
            width: '100%',
            '& h1': {
                width: '100%',
                textAlign: 'center',
            }
        }
    },
    '@media (max-width: 768px)': {
        arrow: {
            display: 'flex'
        }
    },
});

export default styles;