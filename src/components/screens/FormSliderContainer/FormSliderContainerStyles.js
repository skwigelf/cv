const styles = (theme) => ({
    leftMenuPoint: {
        height: '5px',
        width: '5px',
        cursor: 'pointer',
        margin: 'auto',
        backgroundColor: '#000000'
    },
    leftMenuPointWrapper: {
        width: '100%',
        height: '25px',
        display: 'flex',
        alignContent: 'center',
        marginTop: '20px',
        marginBottom: '20px',
        cursor: 'pointer'
    },
    active: {
        width: '40px',
        margin: 'auto',
        backgroundColor: '#9636d5',
        borderRadius: '40px',
        lineHeight: '40px',
        textAlign: 'center',
        color: '#ffffff',
        cursor: 'pointer'
    },
    leftFormMenu: {
        display: 'flex',
        width: '45px',
        backgroundColor: '#e0e0e0',
        flexDirection: 'column',
        borderRadius: '25px'
    },
    formWrapper: {
        width: '675px',
    },
    formContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    formSlider: {
        width: '60%',
    },
    CVPreviewContainer: {
        width: '35%',
        height: '500px',
    },
    formSliderContainer: {
        display: 'flex',
        alignContent: 'space-between',
    },
    formNextStep: {
        display: 'block',
        height: '32px',
        width: '92px',
        textAlign: 'center',
        backgroundColor: '#8f2ecf',
        fontSize: '15px',
        lineHeight: '32px',
        color: '#ffffff',

        marginLeft: '20px',
        marginRight: '20px',
        borderRadius: '5px',
        cursor: 'pointer'
    },
    formPrevStep: {
        display: 'block',
        height: '32px',
        width: '92px',
        textAlign: 'center',
        marginLeft: '20px',
        marginRight: '20px',
        backgroundColor: '#d4dae7',
        fontSize: '15px',
        lineHeight: '32px',
        color: '#ffffff',
        borderRadius: '5px',
        cursor: 'pointer'
    },
    CVPreviewChangeStyleButton: {
        textAlign: 'center',
        color: '#8f2ecf',
    },
    buttonsContainer: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: '15px',
    },
    '@media (max-width: 768px)': {
        formContainer: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
        },
        formSlider: {
            width: '100%',
        },
        CVPreviewContainer: {
            display: 'none',
        },
        leftFormMenu: {
            display: 'flex',
            width: '80%',
            margin: 'auto',
            backgroundColor: '#ffffff',
            flexDirection: 'row',
            borderRadius: '25px'
        },
        formWrapper: {
            width: '100%',
        },
        active: {
            height: '10px',
            width: '10px',
            cursor: 'pointer',
            margin: 'auto',
            backgroundColor: '#9636d5',
        },
        leftMenuPoint: {
            height: '10px',
            width: '10px',
            cursor: 'pointer',
            margin: 'auto',
            borderRadius: '15px',
            backgroundColor: '#000000'
        },
    },
});

export default styles;