import React, {useState} from 'react';
import {Form, Formik} from "formik";
import '../../common/DefaultStyles.css';
import Template1 from "../CVDataForm/Templates/Template1/Template1";
import {changeCVTemplate, submitCV, updateCVData} from "../../../api/CVApi";
import {getTokenFromLocalStorage} from "../../../util/functions";
import styles from "./FormSliderContainerStyles";
import {withStyles} from "@material-ui/core";
import Template2 from "../CVDataForm/Templates/Template2/Template2";

const FormSliderContainer = (props) => {

    const [tab, setTab] = useState(0);
    const [isFormDisabled, setIsFormDisabled] = useState(false);
    const {cvId, data, setData, formTypes, setStatus, classes} = props;

    const token = getTokenFromLocalStorage();
    return (
        <div className={classes.formSliderContainer}>
            <Formik initialValues={data} onSubmit={values => {

            }}>{({values}) => {
                console.log(values);
                const FormTab = formTypes[tab];
                return (<>

                        <Form className={classes.formSlider}>
                            <div className={classes.formContainer}>
                                <div className={classes.leftFormMenuWrapper}>
                                    <div className={classes.leftFormMenu}>
                                        {formTypes.map((key, index, val) => <div
                                            className={classes.leftMenuPointWrapper}
                                            onClick={() =>
                                                setTab(index)
                                            }>
                                            <div
                                                className={isActive(index, tab, classes)}>{index === tab ? index + 1 : ""}</div>
                                        </div>)}
                                    </div>
                                </div>
                                <div className={classes.formWrapper}>
                                    <FormTab values={values}/>
                                    <div className={classes.buttonsContainer}>
                                        {tab !== 0 ?
                                            <a className={classes.formPrevStep} onClick={() => {
                                                setTab(tab - 1);
                                                if (token) {
                                                    updateCVData(cvId, values, token).then((result) => {
                                                    })
                                                }
                                            }
                                            }>
                                                Назад
                                            </a>
                                            :
                                            ""
                                        }
                                        {formTypes.length - 1 === tab ?
                                            <div onClick={() => {
                                                console.log(values);
                                                setIsFormDisabled(true);
                                                if (token) {
                                                    submitCV(cvId, values, token)
                                                        .then(r => {
                                                            setStatus(r.status);
                                                        })
                                                } else {
                                                    setData(values);
                                                    setStatus("CREATED");
                                                }
                                            }} className={classes.formNextStep}>Создать резюме</div>
                                            :
                                            <a className={classes.formNextStep} onClick={() => {
                                                setTab(tab + 1);
                                                if (token) {
                                                    updateCVData(cvId, values, token).then((result) => {
                                                    })
                                                } else {
                                                    setData(values);
                                                }
                                            }
                                            }>
                                                Далее
                                            </a>
                                        }
                                    </div>
                                </div>
                            </div>

                        </Form>

                        <div className={classes.CVPreviewContainer}>
                            <Template2 values={values}/>
                            <div className={classes.CVPreviewChangeStyleButton} onClick={() => {
                                changeCVTemplate(cvId, token)
                                    .then(r => {
                                        setStatus("NEW");
                                    })
                            }}>Изменить шаблон
                            </div>
                        </div>
                    </>
                )
            }}
            </Formik>

        </div>
    )
};

const isActive = (state, value, classes) => {
    return state === value ? classes.active : classes.leftMenuPoint;
};

export default withStyles(styles)(FormSliderContainer);
