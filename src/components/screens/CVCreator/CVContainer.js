import React, {useEffect, useState} from 'react';
import '../../common/DefaultStyles.css';
import {useParams} from "react-router";
import CVDataForm from "../CVDataForm/CVDataForm";
import PurchaseScreen from "../PurchaseScreen/PurchaseScreen";
import ChooseTemplate from "../ChooseTemplate/ChooseTemplate";
import {getCV} from "../../../api/CVApi";
import {getTokenFromLocalStorage} from "../../../util/functions";
import {getUserInfoApi} from "../../../api/UserApi";
import NotFound from "../ErrorPage/NotFound";

const CVContainer = (props) => {
    let {id} = useParams();
    const [cv, setCv] = useState({});
    const [component, setComponent] = useState(<div/>);
    const [status, setStatus] = useState("");
    const token = getTokenFromLocalStorage();

    const getComponentByStatus = (status, data) => {
        if (status === "NEW") {
            return <ChooseTemplate cvId={id} cv={cv} token={token} setStatus={setStatus}/>;
        }
        if (status === "FILL_DATA") {
            return <CVDataForm cvId={id} data={setEducationsIfEmpty(formData(data))} setStatus={setStatus}/>;
        }
        if (status === "CREATED") {
            return <PurchaseScreen cvId={id} data={formData(data)} setStatus={setStatus}/>
        }
        return <div/>
    };

    useEffect(() => {
        if (token) {
            getCV(id, token).then((result) => {
                const data = result.data;
                console.log(result.data);
                console.log(data && data.educations && data.educations.length < 1);


                setCv(result);
                setComponent(getComponentByStatus(result.status, data));
                setStatus(result.status);
            }).catch((result) => {
                console.log(result)
                setComponent(<NotFound/>)
            })
        }
    }, [status]);

    return (
        <>
            {component}
        </>
    );
};

const formData = (data) => data ? JSON.parse(data) : {
    user: {
        firstName: 'FirstName',
        lastName: 'LastName',
        birthDate: '',
        phone: '',
        email: '',
    },
    educations: [{type: 'HIGH'}],
    experiences: [{}],
    hobbies: [],
    interests: [],
    extraInfo: {}
};

const setEducationsIfEmpty = (data) => {
    console.log(data);
    if (data && data.educations && data.educations.length === 0) {
        Object.assign(data.educations, [{type: "HIGH"}]);
    }
    return data;
};


export default CVContainer;
