import React, {useEffect, useState} from 'react';
import '../../common/DefaultStyles.css';
import CVDataForm from "../CVDataForm/CVDataForm";
import PurchaseScreen from "../PurchaseScreen/PurchaseScreen";
import ChooseTemplate from "../ChooseTemplate/ChooseTemplate";
import {getTokenFromLocalStorage} from "../../../util/functions";

const CVContainer = (props) => {
    const [cv, setCv] = useState({});
    const [data, setData] = useState('');
    const [templateId, setTemplateId] = useState({});
    const [component, setComponent] = useState(<div/>);
    const [status, setStatus] = useState("NEW");
    const token = getTokenFromLocalStorage();

    const getComponentByStatus = (status, data) => {
        if (status === "NEW") {
            return <ChooseTemplate cv={cv} token={token} setStatus={setStatus} setTemplateId={setTemplateId}/>;
        }
        if (status === "FILL_DATA") {
            return <CVDataForm data={formData(data)} setData={setData} templateId={templateId}
                               setStatus={setStatus}/>;
        }
        if (status === "CREATED") {
            return <PurchaseScreen data={formData(data)} setData={setData} templateId={templateId}
                                   setStatus={setStatus}/>
        }
        return <div/>
    };

    useEffect(() => {
        setComponent(getComponentByStatus(status, data));
    }, [status]);

    return (
        <>
            {component}
        </>
    );
};

const formData = (data) => data ? data : {
    user: {
        firstName: 'FirstName',
        lastName: 'LastName',
        birthDate: '',
        phone: '',
        email: '',
    },
    educations: [{type: 'HIGH'}],
    experiences: [{}],
    hobbies: [],
    interests: [],
    extraInfo: {}
};


export default CVContainer;
