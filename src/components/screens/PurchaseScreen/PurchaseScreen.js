import React, {useState} from 'react';
import '../../common/DefaultStyles.css';
import './PurchaseScreen.css';
import EmailModal from "./EmailModal/EmailModal";
import {changeCVTemplate, getCVDocument} from "../../../api/CVApi";
import {getTokenFromLocalStorage} from "../../../util/functions";
import Template2 from "../CVDataForm/Templates/Template2/Template2";
import {createPdf, createPdfNoAuth} from "../../../api/CVPdf";
import {BlobProvider, PDFDownloadLink} from '@react-pdf/renderer'
import {withStyles} from "@material-ui/core";
import styles from "./PurchaseScreenStyles";
import LoginModal from "../LoginModal/LoginModal";

const PurchaseScreen = (props) => {

    const {data, setStatus, cvId, templateId, classes} = props;
    const [modal, setModal] = useState(false);
    const [emailModal, setEmailModal] = useState(false);
    const token = getTokenFromLocalStorage();
    const handleModal = () => {
        setModal(!modal);
    };
    const handleLoginModal = () => {
        setEmailModal(!emailModal);
    };

    return (
        <>
            <div className={"screen-container"}>

                <div className={classes.purchaseTitleContainer}>
                    <h1>Step 3. Финальные штрихи</h1>
                </div>


                <div className={classes.purchaseContainer}>
                    {emailModal ? <><EmailModal handleModal={handleLoginModal}/><div onClick={() => setEmailModal(!emailModal)} className={classes.otherScreen}/></> : ""}
                    <div className={classes.purchaseLeftContainer}>
                        <div className={classes.purchaseBody}>
                            <div className={classes.purchaseLeftContainer}>
                                <div className={classes.purchaseCongratTextContainer}>
                                    <span>Поздравляем! Твое резюме готово. Теперь ты можешь распечатать его или скачать</span>
                                </div>
                                <div className={classes.purchaseSaveTemplateButtonContainer}>
                                    <div className={classes.purchaseSaveTemplateButton}
                                         onClick={() => {
                                             if (token) {
                                                 getCVDocument(cvId, token);
                                             } else {
                                                 createPdf(data, templateId);
                                             }
                                         }}>Открыть в новой вкладке
                                    </div>
                                    <div className={classes.purchaseSaveTemplateButton}
                                         onClick={() => {
                                             if (token) {
                                                 getCVDocument(cvId, token);
                                             } else {
                                                 createPdfNoAuth(data, templateId);
                                             }
                                         }}>Сохранить
                                    </div>
                                    <div className={classes.purchaseSaveTemplateButton}
                                         onClick={handleLoginModal}>Отправить на почту
                                    </div>
                                </div>
                                <div className={classes.purchaseProductDescriptionTextContainer}>
                                    <span>Your CV is great but if we tell you it could be THE GREAT? Our AI based system can enchance it and create</span><br/><br/>
                                    <span>Moreover we can give you access to biggest base of current open positions ideals for your skillset</span>
                                </div>
                                <h4 className={classes.purchaseChooseYourself}>Choose yourself</h4>
                                <div className={classes.purchaseBottomButtonContainer}>
                                    <a href={"https://www.youtube.com/watch?v=4EknBt0cXdE"}
                                       className={classes.purchaseSaveTemplateButton}>Use now</a>
                                    <div className={classes.purchaseSaveTemplateButton}>Learn more</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={classes.purchaseRightContainer}>
                        <div className={classes.purchaseRightContent}>
                            <Template2 values={data}/>
                            <div className={classes.CVPreviewChangeStyleButton} onClick={() => {
                                changeCVTemplate(cvId, token)
                                    .then(r => {
                                        setStatus("NEW");
                                    })
                            }}>Изменить шаблон
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </>
    );
};

export default withStyles(styles)(PurchaseScreen);
