import React, {useState} from 'react';
import Modal from "../../../common/Modal/Modal";
import './EmailModal.css';

const EmailModal = (props) => {
    const {handleModal} = props;

    return (
        <Modal content={
            <div>
                <div className={"email-modal-body-container"}>
                    <span>Where you would like to send your CV?</span>
                    <div className={"email-modal-input-wrappers"}>
                        <input type="email" id={"email-modal-email-input"}/>
                    </div>
                    <div className={"email-modal-send-button"}>
                        Send
                    </div>
                    <input id={"isSend"} type="checkbox"/>
                    <label htmlFor="isSend">By the way, may we send you our news? Its pretty cool</label>
                </div>

            </div>
        }
               handleModal={handleModal}/>
    );
};

export default EmailModal;
