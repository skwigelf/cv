const styles = (theme) => ({
    purchaseContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    purchaseLeftContainer: {
        width: '85%',
    },
    purchaseRightContainer: {
        width: '85%',
    },
    purchaseTitleContainer: {

    },
    purchaseSaveTemplateButtonContainer: {
        paddingTop: '20px',
        paddingBottom: '20px',
        display: 'flex',
        justifyContent: 'space-between',
    },
    purchaseSaveTemplateButton: {
        color: '#9636d5',
    },
    purchaseBottomButtonContainer: {
        display: 'flex',
        justifyContent: 'space-around',
        paddingTop: '20px',
        paddingBottom: '30px',
    },
    purchaseChooseYourself: {
        textAlign: 'center',
    },
    otherScreen: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        opacity: '0.8',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        top: '0',
        left: '0',
        zIndex: '100',
    },
    purchaseProductDescriptionTextContainer: {},
    '@media (max-width: 768px)': {
        purchaseContainer: {
            display: 'flex',
            justifyContent: 'space-between',
            flexDirection: 'column-reverse'
        },
        purchaseTitleContainer: {
            textAlign: 'center',
        },
        purchaseRightContainer: {
            width: '100%',
        },
        purchaseSaveTemplateButton: {
            color: '#9636d5',
            width: '30vw',
        },
        purchaseLeftContainer: {
            width: '100%',
            paddingLeft: '5vw',
            paddingRight: '5vw',
            boxSizing: 'border-box',
            marginTop: '15px',
        },
    },
    CVPreviewChangeStyleButton: {
        textAlign: 'center',
        color: '#8f2ecf',
    },

});

export default styles;