import React from 'react';
import '../../common/DefaultStyles.css';
import Modal from "../../common/Modal/Modal";
import {Field, Form, Formik} from "formik";
import {loginApi, registerApi} from "../../../api/UserApi";
import {useHistory} from "react-router";
import {Link} from "react-router-dom";
import './RegisterModal.css';

const RegisterModal = (props) => {
    const {handleModal} = props;
    const {history} = useHistory();
    return (
        <Modal handleModal={handleModal} content={
            <div className={"register-modal-container"}>
                <div className={"register-modal-form-container"}>
                    <Formik initialValues={{
                        username: '',
                        password: '',
                        email: ''
                    }} onSubmit={(values) => {
                        registerApi(values)
                            .then(() => {
                                handleModal();
                            })
                            .catch((result) => {
                            });
                    }}>
                        <Form>
                            <Field id="email" type={"email"} name={`email`}
                                   className={"input-user-default"}
                                   placeholder={"email"}/>
                            <Field id="username" type={"login"} name={`username`}
                                   className={"input-user-default"}
                                   placeholder={"имя пользователя"}/>
                            <Field id="password" type={"password"} name={`password`}
                                   className={"input-user-default"}
                                   placeholder={"пароль"}/>
                            <button type={"submit"} className={"modal-register-button"}>
                                Регистрация
                            </button>
                        </Form>
                    </Formik>
                </div>

            </div>

        }/>
    );
};


export default RegisterModal;
