import React, {useEffect, useRef, useState} from 'react';
import {withStyles} from "@material-ui/core";
import styles from "./LoginStyles";
import {Field, Form, Formik} from "formik";
import {loginApi} from "../../../api/UserApi";
import jwt_decode from "jwt-decode";
import {Link} from "react-router-dom";

const Login = (props) => {

    const {classes} = props;
    return (
        <>
            <div className={classes.headerStyleContainer}>
                <span className={classes.headerStyle}><b className={classes.boldLetters}>CV</b>eria</span>
            </div>

            <div className={classes.loginModalContainer}>
                <div className={classes.formContainer}>
                    <Formik initialValues={{
                        username: '',
                        password: ''
                    }} onSubmit={(values) => {
                        loginApi(values).then((result) => {
                            const token = result.token;
                            const user = jwt_decode(token).iss;
                            localStorage.setItem("token", token);
                            localStorage.setItem("user", user);
                            console.log("FUCK");
                            //setLoginError(false);
                        })
                            .catch((result) => {
                                //  setLoginError(true);
                            });
                    }}>
                        <Form className={classes.loginForm}>
                            <label htmlFor={"username"} className={classes.inputLabel}>Email</label>
                            <Field id="username" type={"login"} name={`username`}
                                   className={classes.loginInput}
                                   placeholder={"E-mail"}/>
                            <label htmlFor={"password"} className={"labelClassName"}>Password</label>
                            <Field id="password" type={"password"} name={`password`}
                                   className={classes.loginInput}
                                   placeholder={"Pass"}/>
                            <div className={classes.forgotPasswordContainer}>
                                <Link to={"/register"}>
                                    Forgot password?
                                </Link>
                            </div>

                            <button type={"submit"} className={classes.loginButton}>
                                Sign in
                            </button>
                            <button type={"submit"} className={classes.signUpButton}>
                                Sign up
                            </button>
                        </Form>
                    </Formik>
                </div>

            </div>
        </>
    );
};


export default withStyles(styles)(Login);
