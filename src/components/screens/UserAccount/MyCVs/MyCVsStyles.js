const styles = (theme) => ({
    container: {
        background: '#ffffff',
        width: '900px',
        display: 'flex',
        padding: '50px',
        alignContent: 'flex-start',
        justifyContent: 'flex-start',
        flexWrap: 'wrap'
    },
    createTitle: {
        color: '#883eb0',
        cursor: 'pointer',
    },
    previewContainer: {
        margin: 'auto',
        width: '300px'
    },
    pagingPoint: {
        marginLeft: '5px',
    },
    activePoint: {
        color: '#883eb0',
    }
});

export default styles;