import React from 'react';
import '../../../../common/DefaultStyles.css';
import {withStyles} from "@material-ui/core";
import styles from './CVItemStyles';
import {useHistory} from "react-router";

const CVItem = (props) => {

    const {status, cvId, title, description, image, classes} = props;
    const imageSrc = image ? `data:image/jpg;base64,${image}` : "/camry.jpg";
    const history = useHistory();
    const handleClick = () => {
        history.push('/cv/' + cvId);
    };
    return (
        <div className={classes.templateItemContainer}>
            <div className={classes.templateItemImageContainer}>
                <img src={imageSrc} alt=""/>
            </div>
            <div className={classes.templateItemTextContainer}>
                <div onClick={handleClick} className={"default-link"}><h3>{title}</h3></div>
                <div className={classes.templateItemLine}/>
                <p className={classes.description}>{description}</p>
                <span className={classes.status}>{status}</span>
            </div>
        </div>
    );
};

export default withStyles(styles)(CVItem);
