const styles = (theme) => ({
    templateItemContainer: {
        width: '50%',
        display: 'flex',
    },
    templateItemImageContainer: {
        borderRadius: '4px',
        '& img': {
            width: '120px',
            height: '180px',
            borderRadius: '15px',
            marginRight: '30px',
        }
    },
    templateItemLine: {
        width: '190px',
        height: '2px',
        paddingLeft: '3px',
        paddingRight: '3px',
        backgroundColor: '#9636d5',
    },
    status: {
        color: '#883eb0',
    },
});

export default styles;