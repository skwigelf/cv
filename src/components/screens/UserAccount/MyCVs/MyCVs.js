import React, {useEffect, useState} from 'react';
import '../../../common/DefaultStyles.css';
import styles from "./MyCVsStyles";
import {withStyles} from "@material-ui/core";
import {createCV, getCVs} from "../../../../api/CVApi";
import CVItem from "./CVItem/CVItem";
import {getTokenFromLocalStorage} from "../../../../util/functions";
import {useHistory} from "react-router";

const PAGE_NUMBER = 6;

const MyCVs = (props) => {

    const {classes} = props;
    const [cvs, setCvs] = useState([]);
    const [page, setPage] = useState(1);
    const [error, setError] = useState(false);
    const token = getTokenFromLocalStorage();
    const history = useHistory();

    const handleCreateCV = () => {
        createCV(token).then((result) => {
            history.push("/cv/" + result);
        });

    };

    const getStatusText = (status) => {
        if (status === "CREATED") {
            return "Резюме создано";
        }
        if (status === "NEW") {
            return "Step 1. Выбор шаблона";
        }
        if (status === "FILL_DATA") {
            return "Step 2. Заполнение данных";
        }
        return "";
    };
    console.log(cvs);
    useEffect(() => {
        getCVs(token).then((result) => {
            setCvs(result);
        })
            .catch((result) => {
                setError(true)
            })
    }, []);
    return (
        <div className={classes.container}>
            {cvs && cvs.length !== 0
                ? cvs.slice(PAGE_NUMBER * (page), PAGE_NUMBER * (page + 1)).map((key, index, val) => {
                    const template = key.template ? key.template : {
                        title: "Шаблон не выбран",
                        description: "На те камри 3 и 5",
                        previewImage: ""
                    };
                    return (<CVItem cvId={key.id}
                                    status={getStatusText(key.status)}
                                    title={template.title}
                                    description={template.description}
                                    image={template.previewImage}/>)
                })
                :
                error ? <span>У вас нет резюме. <span className={classes.createTitle}
                                                      onClick={handleCreateCV}>Создать?</span></span>
                    : <div className={classes.previewContainer}><img src="/logo.gif" alt=""/></div>
            }
            {cvs && cvs.length !== 0
                ? [...Array(Math.floor(cvs.length / PAGE_NUMBER)).keys()].map(i => <div
                    className={pointClass(classes, page, i)}
                    onClick={() => setPage(i)}>{i + 1}</div>)
                : ""
            }
        </div>
    );
};

const pointClass = (classes, page, i) => {
    console.log(classes.pagingPoint + " " + (page === i ? classes.activePoint : ""));
  return classes.pagingPoint + " " + (page === i ? classes.activePoint : "");
};


export default withStyles(styles)(MyCVs);
