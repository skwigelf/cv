import React, {useEffect, useState} from 'react';
import '../../common/DefaultStyles.css';
import AccountSliderContainer from "./AccountSliderContainer/AccountSliderContainer";
import ContactInformation from "../CVDataForm/forms/ContactInformation/ContactInformation";
import EducationTab from "../CVDataForm/forms/Education/EducationTab";
import WorkExperienceTab from "../CVDataForm/forms/WorkExperience/WorkExperienceTab";
import styles from "./UserAccountStyles";
import {withStyles} from "@material-ui/core";
import MyCVs from "./MyCVs/MyCVs";
import {getUserInfoApi} from "../../../api/UserApi";
import {getTokenFromLocalStorage} from "../../../util/functions";
import HobbiesInterests from "../CVDataForm/forms/HobbiesInterests/HobbiesInterests";

const UserAccount = (props) => {

    const token = getTokenFromLocalStorage();
    const {classes} = props;
    const [isLoaded, setIsLoaded] = useState(false);
    const formTypes = [{component: ContactInformation, title: "О себе", options: {isUserAccount: true}},
        {component: EducationTab, title: "Образование", options: {isUserAccount: true}},
        {component: WorkExperienceTab, title: "Опыт работы", options: {isUserAccount: true}},
        {component: HobbiesInterests, title: "Хобби и навыки", options: {isUserAccount: true}},
        {component: MyCVs, title: "Мои резюме", options: {isUserAccount: true}}];
    const [data, setData] = useState({});

    useEffect(() => {
        getUserInfoApi(token)
            .then((response) => {
                setData(response);
                setIsLoaded(true);
            })
            .catch(() => {})
    }, []);

    return (
        <div className={classes.container}>
            <div className={"screen-container"}>
                <AccountSliderContainer data={data} formTypes={formTypes} token={token} isLoaded={isLoaded}/>
            </div>
        </div>
    );
};

export default withStyles(styles)(UserAccount);
