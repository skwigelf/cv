const styles = (theme) => ({
    container: {
        background: 'url("./acc.png") no-repeat',
        backgroundSize: 'cover',
        width: '100%',
        paddingTop: '50px',
        paddingLeft: '35px',
        paddingRight: '35px',
        //boxSizing: 'border-box',
        paddingBottom: '100px',
        // height: '100%',
        overflowY: 'hidden',
    },
    '@media (max-width: 768px)': {
        container: {
            background: '#ffffff',
            width: '100%',
            //boxSizing: 'border-box',
            padding: '0',
            // height: '100%',
        },

    }

});

export default styles;