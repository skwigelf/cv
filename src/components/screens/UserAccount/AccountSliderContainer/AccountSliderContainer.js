import React, {useState} from 'react';
import {Field, Form, Formik} from "formik";
import '../../../common/DefaultStyles.css';
import styles from "./AccountSliderContainerStyles";
import {withStyles} from "@material-ui/core";
import {userInfoUpdateApi} from "../../../../api/UserApi";
import MyCVs from "../MyCVs/MyCVs";

const AccountSliderContainer = (props) => {

    const [tab, setTab] = useState(0);
    const {cvId, data, formTypes, setStatus, isLoaded, token, classes} = props;

    return (<>
            {
                isLoaded ?
                    <div className={classes.formSliderContainer}>
                        <Formik initialValues={setEducationsIfEmpty(data)} onSubmit={values => {

                        }}>{({values}) => {
                            console.log(formTypes);
                            console.log(tab);
                            const FormTab = formTypes[tab].component;
                            console.log(formTypes[tab].options.isUserAccount);
                            return (<>

                                    <Form className={classes.formSlider}>
                                        <div className={classes.formContainer}>
                                            <div className={classes.leftFormMenuWrapper}>
                                                <div className={classes.leftFormMenu}>
                                                    {formTypes.map((key, index, val) => <div
                                                        className={classes.leftMenuPointWrapper}
                                                        onClick={() =>
                                                            setTab(index)
                                                        }>
                                                        <div
                                                            className={isActive(index, tab, classes)}>
                                                            {formTypes[index].title}</div>
                                                    </div>)}
                                                </div>
                                                <div className={classes.mobileMenu}>
                                                    <h3 className={classes.mobileMenuHeader}>Мой профиль</h3>
                                                    <select
                                                        className={"input-user-default"}
                                                        value={tab}
                                                        onChange={(event) =>
                                                            setTab(event.target.value)
                                                        }
                                                    >
                                                        {formTypes.map((key, index, val) => <option
                                                            className={classes.leftMenuPointWrapper}
                                                            value={index}
                                                            >
                                                            {formTypes[index].title}
                                                        </option>)}
                                                    </select>
                                                </div>
                                            </div>
                                            <div className={classes.formWrapper}>
                                                <div className={classes.formHeaderContainer}>
                                                    <span
                                                        className={classes.formHeaderStyle}>{formTypes[tab].title}</span>
                                                </div>
                                                <FormTab values={values}
                                                         isUserAccount={formTypes[tab].options.isUserAccount}/>
                                                {formTypes[tab].component !== MyCVs ? <div className={classes.submitButton} onClick={
                                                    () => userInfoUpdateApi(token, values)
                                                        .then(() => {

                                                        })
                                                        .catch(() => {
                                                        })
                                                }>
                                                    Обновить данные
                                                </div> : ""}
                                            </div>
                                        </div>

                                    </Form>
                                </>
                            )
                        }}
                        </Formik>

                    </div>
                    :
                    <><div className={classes.previewContainer}><img src="/logo.gif" alt=""/></div></>
            }
        </>
    )
};

const isActive = (state, value, classes) => {
    return state === value ? classes.menuActive : classes.leftMenuPoint;
};

const setEducationsIfEmpty = (data) => {
    console.log(data);
    if (data && data.educations && data.educations.length === 0) {
        Object.assign(data.educations, [{type: "HIGH"}]);
    }
    return data;
};

export default withStyles(styles)(AccountSliderContainer);
