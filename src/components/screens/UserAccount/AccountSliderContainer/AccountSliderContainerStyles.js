const styles = (theme) => ({
    leftMenuPoint: {
        paddingLeft: '15px',
        paddingRight: '15px',
        paddingTop: '15px',
        paddingBottom: '15px',
        cursor: 'pointer',
    },
    leftMenuPointWrapper: {
        width: '100%',
        display: 'flex',
        alignContent: 'center',
        cursor: 'pointer',
        //paddingTop: '5px',
        //paddingBottom: '5px',
        '& div:hover': {
            backgroundColor: '#883eb0',
            paddingLeft: '20px',
            paddingRight: '20px',
            paddingTop: '15px',
            paddingBottom: '15px',
            color: '#ffffff',
            width: '100%',
            cursor: 'pointer'
        }
    },
    menuActive: {
        backgroundColor: '#883eb0',
        paddingLeft: '20px',
        paddingRight: '20px',
        paddingTop: '15px',
        paddingBottom: '15px',
        color: '#ffffff',
        width: '100%',
        cursor: 'pointer'
    },
    leftFormMenu: {
        display: 'flex',
        width: '220px',
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderRadius: '10px',
        borderWidth: '2px',
        borderColor: '#002200',
        marginRight: '50px',
        paddingTop: '15px',
        paddingBottom: '15px',
        //paddingLeft: '20px',
        boxSizing: 'border-box',
        boxShadow: '0 0 10px rgba(0,0,0,0.5)',
    },
    leftFormMenuWrapper: {
        zIndex: '200',
        //borderRadius: '15px',
    },
    mobileMenu: {
        display: 'none',
    },
    formWrapper: {
        width: '650px',
        marginLeft: '-75px',
        zIndex: '150',
    },
    formContainer: {
        display: 'flex',
        justifyContent: 'flex-start',
    },
    formSlider: {
        width: '95%',
    },
    mobileMenuHeader: {
        textAlign: 'center',
    },
    CVPreviewContainer: {
        width: '45%',
        height: '500px',
    },
    formSliderContainer: {
        display: 'flex',
        alignContent: 'space-between',
    },
    previewContainer: {
        margin: 'auto',
        paddingBottom: '250px',
        '& img': {
            width: '250px',
            height: '250px',
            margin: 'auto',
            display: 'block'
        }
    },
    submitButton: {
        backgroundColor: '#883eb0',
        color: '#ffffff',
        width: '200px',
        padding: '5px',
        borderRadius: '5px',
        marginTop: '10px',
        marginLeft : '10px',
    },
    formHeaderContainer: {
        marginTop: '40px',
        marginBottom: '25px',
        paddingLeft: '50px',
    },
    formHeaderStyle: {
        color: '#ffffff',
        fontWeight: '10px',
        fontSize: '25px',
    },
    '@media (max-width: 1000px)': {
        formWrapper: {
            width: '70%',
        },
    },
    '@media (max-width: 768px)': {
        formContainer: {
            display: 'flex',
            justifyContent: 'flex-start',
            flexDirection: 'column',
        },
        formWrapper: {
            width: '100%',
            marginLeft: '0',
            //zIndex: '150',
        },
        formSlider: {
          width: '100%',
        },
        mobileMenu: {
            paddingLeft: '50px',
            paddingRight: '50px',
            display: 'block',
        },
        leftFormMenu: {
            display: 'none',
        },
        formHeaderContainer: {
            display: 'none',
        },
    }
});

export default styles;