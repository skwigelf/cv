const styles = (theme) => ({
    formContainer: {
        paddingTop: '10px',
        width: '80%',
        margin: 'auto',
        paddingBottom: '50px',
    },
    loginButton: {
        backgroundColor: '#883eb0',
        textAlign: 'center',
        //paddingTop: '10px',
        //paddingBottom: '10px',
        height: '34px',
        width: '110px',
        margin: 'auto',
        display: 'block',
        color: '#ffffff',
        fontSize: '15px',
        textDecoration: 'none',
        border: 'none',
        cursor: 'pointer',
        borderRadius: '5px',
    },
    headerStyle: {

    },
    headerStyleContainer: {
        textAlign: 'center',
        color: '#8942d4',
        fontSize: '38px',
        marginBottom: '30px',
    },
    logoContainer: {
        width: '100px',
        margin: 'auto'
    },
    boldLetters: {
        fontWeight: '25px'
    },
    loginInput: {
        height: '45px',
        lineHeight: '45px',
        paddingLeft: '15px',
        display: 'block',
        width: '100%',
        background: '#c4c6c7',
        border: '1px solid #000000',
        borderRadius: '10px',
        marginTop: '5px',
        marginBottom: '25px',
        boxSizing: 'border-box',
    },
    loginForm: {
        width: '340px',
        margin: 'auto',
    },
    inputLabel: {
        marginTop: '5px',
        marginBottom: '5px'
    },
    forgotPasswordContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginBottom: '30px'
    },
    signUpButton: {
        marginTop: '15px',
        backgroundColor: '#ffffff',
        textAlign: 'center',
        paddingTop: '10px',
        paddingBottom: '10px',
        width: '150px',
        margin: 'auto',
        display: 'block',
        color: '#883eb0',
        fontSize: '15px',
        textDecoration: 'none',
        border: 'none',
        cursor: 'pointer',
        borderRadius: '5px',
    }
});

export default styles;