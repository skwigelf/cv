import React from 'react';
import '../../common/DefaultStyles.css';
import './ErrorPage.css';
import '../../common/DefaultStyles.css';

const ErrorPage = (props) => {

    const {title, text} = props;
    return (
        <div className={"screen-container"}>
            <div className={"error-header"}>
                <h1>{title}</h1>
            </div>
            <div className={"error-content"}>
                <span>{text}</span>
            </div>
        </div>
    );
};

export default ErrorPage;
