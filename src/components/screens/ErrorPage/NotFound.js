import React from 'react';
import ErrorPage from "./ErrorPage";
import {Link} from "react-router-dom";

const NotFound = (props) => {

    return (
        <ErrorPage title={"Page not found"} text={
            <>
                Такой страницы нет. Перейти на <Link to={"/"}>главную</Link>
            </>
        }/>
    );
};

export default NotFound;
