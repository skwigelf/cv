import React, {useState} from 'react';
import './FormTab.css'

const FormTab = (props) => {
    const formContent = props.content;
    const [hideButton, setHideButton] = useState("");

    return (
        <section className={"section-wrapper"}>
            <div className={"section-form-wrapper"}>

                <div className={"line"}/>
                <div className={hideButton}>
                    {formContent}
                </div>
            </div>
        </section>
    )
};

export default FormTab;