import React, {useState} from 'react';
import './style.css';

const Template1 = (props) => {
    const values = props.values;
    return (

        <div className={"temp-cont"}>
            <div className="stl_02">
                <div className="stl_view">
                    <div className="stl_05 stl_06">
                        <div className="stl_01" style={{left: '3em', top: '6.6925em'}}><span
                            className="stl_07 stl_08 stl_09">{values.user.firstName} &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '3.0417em', top: '8.8425em'}}><span
                            className="stl_10 stl_08 stl_11"
                            style={{wordSpacing: '-0.0353em'}}>{values.user.lastName} &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '2.9583em', top: '18.9667em'}}><span
                            className="stl_12 stl_08 stl_11"
                            style={{wordSpacing: '0.0347em'}}>C O N T A C T &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '18.8em', top: '18.9712em'}}><span
                            className="stl_12 stl_08 stl_11"
                            style={{wordSpacing: '0.053em'}}>P R O F I L E &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '3.7083em', top: '21.9615em'}}><span
                            className="stl_13 stl_08 stl_14"
                            style={{wordSpacing: '0.017em'}}>Avenue Park NY-0123-USA &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '3.7083em', top: '23.1908em'}}><span
                            className="stl_13 stl_08 stl_15"
                            style={{wordSpacing: '0.0094em'}}>{values.user.phone} &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '18.7997em', top: '21.9615em'}}><span
                            className="stl_13 stl_08 stl_16" style={{wordSpacing: '0.0018em'}}>Itae audi voluptatum audandant rest acilign atemporpore nes non pratemp &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '18.7997em', top: '22.7615em'}}><span
                            className="stl_13 stl_08 stl_17" style={{wordSpacing: '-0.0051em'}}>orest, tem quost aut vernatius doluptae volum vellorrum inctaque ommolor &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '18.7997em', top: '23.5615em'}}><span
                            className="stl_13 stl_08 stl_18" style={{wordSpacing: '-0.0004em'}}>porehenit plictur as et, sit liquasped quiatquam, sectur? Quiatis itatis eiciet &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '18.7997em', top: '24.3615em'}}><span
                            className="stl_13 stl_08 stl_19" style={{wordSpacing: '0.0034em'}}>aut et fuga. Itamuscim reptasp. &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '3.7083em', top: '24.3908em'}}><a
                            href="mailto:sam.william@gmail.com" target="_blank"><span
                            className="stl_13 stl_08 stl_20">{values.user.email} &nbsp;</span></a></div>
                        <div className="stl_01" style={{left: '2.9583em', top: '26.0215em'}}><span
                            className="stl_12 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>S K I L L S &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '2.9583em', top: '28.2215em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '2.9583em', top: '29.7215em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '2.9583em', top: '31.2215em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '2.9583em', top: '32.7215em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '2.9583em', top: '34.2215em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '4.4583em', top: '28.2215em'}}><span
                            className="stl_13 stl_08 stl_21"
                            style={{wordSpacing: '0.0013em'}}>Graphic Design &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '18.8em', top: '28.6091em'}}><span
                            className="stl_12 stl_08 stl_11"
                            style={{wordSpacing: '0.053em'}}>E X P E R I E N C E &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '4.4583em', top: '29.7215em'}}><span
                            className="stl_13 stl_08 stl_22"
                            style={{wordSpacing: '-0.012em'}}>UI/UX Design &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '4.4583em', top: '31.2215em'}}><span
                            className="stl_13 stl_08 stl_23" style={{wordSpacing: '0.0183em'}}>Web Design &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.2em', top: '31.4075em'}}><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>J O B</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.572em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>T I T L E</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>H E R E</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>| 2</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>0 3 4</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>- P</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.05em'}}>R E S E N T &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.2em', top: '33.305em'}}><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.0443em'}}>C O M P A N Y</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.566em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>N A M E &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '34.5928em'}}><span
                            className="stl_13 stl_08 stl_16" style={{wordSpacing: '0.0018em'}}>Itae audi voluptatum audandant rest acilign atemporpore nes non pratemp &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '35.3928em'}}><span
                            className="stl_13 stl_08 stl_17" style={{wordSpacing: '-0.0051em'}}>orest, tem quost aut vernatius doluptae volum vellorrum inctaque ommolor &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '36.1928em'}}><span
                            className="stl_13 stl_08 stl_25"
                            style={{wordSpacing: '-0.0045em'}}>porehenit plictur as et. &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '4.4583em', top: '32.7215em'}}><span
                            className="stl_13 stl_08 stl_16"
                            style={{wordSpacing: '0.0048em'}}>Project Management &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '4.4583em', top: '34.2215em'}}><span
                            className="stl_13 stl_08 stl_26"
                            style={{wordSpacing: '0.0128em'}}>Market Research &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '8.6419em', top: '36.2985em'}}><span
                            className="stl_12 stl_08 stl_11"
                            style={{wordSpacing: '0.0506em'}}>I N T E R E S T S &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '36.9928em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '37.7928em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '38.5928em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '21.6997em', top: '36.9928em'}}><span
                            className="stl_13 stl_08 stl_27" style={{wordSpacing: '-0.0062em'}}>Por a duntur aut qui omnienist amene nobitatur Quia si aut &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '21.6997em', top: '37.7928em'}}><span
                            className="stl_13 stl_08 stl_28" style={{wordSpacing: '-0.0046em'}}>molute vel exerior susdaes volupta ectiis et liquas solorep tatur. &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '21.6997em', top: '38.5928em'}}><span
                            className="stl_13 stl_08 stl_29" style={{wordSpacing: '0.0018em'}}>Quiae eaquis apienet rerro evenimus parunt magnihil modiatur. &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '8.6419em', top: '38.6186em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '8.6419em', top: '40.1186em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '8.6419em', top: '41.6186em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '8.6419em', top: '43.1186em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '8.6419em', top: '44.6186em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '8.6419em', top: '46.1186em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '8.6419em', top: '47.6186em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '8.6419em', top: '49.1186em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '10.1419em', top: '38.6186em'}}><span
                            className="stl_13 stl_08 stl_30">Photography &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '10.1419em', top: '40.1186em'}}><span
                            className="stl_13 stl_08 stl_16">Swimming &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '10.1419em', top: '41.6186em'}}><span
                            className="stl_13 stl_08 stl_18">Riding &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '10.1419em', top: '43.1186em'}}><span
                            className="stl_13 stl_08 stl_31">T</span><span
                            className="stl_13 stl_08 stl_32">r</span><span
                            className="stl_13 stl_08 stl_33">avel &nbsp;</span></div>
                        <div className="stl_01" style={{left: '20.2em', top: '43.6233em'}}><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>J O B</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.572em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>T I T L E</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>H E R E</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>| 2</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>0 3 0</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>- 2</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>0 3 4 &nbsp;</span></div>
                        <div className="stl_01" style={{left: '20.2em', top: '45.5208em'}}><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.0443em'}}>C O M P A N Y</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.566em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>N A M E &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '46.8081em'}}><span
                            className="stl_13 stl_08 stl_16" style={{wordSpacing: '0.0018em'}}>Itae audi voluptatum audandant rest acilign atemporpore nes non pratemp &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '47.6081em'}}><span
                            className="stl_13 stl_08 stl_17" style={{wordSpacing: '-0.0051em'}}>orest, tem quost aut vernatius doluptae volum vellorrum inctaque ommolor &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '48.4081em'}}><span
                            className="stl_13 stl_08 stl_25"
                            style={{wordSpacing: '-0.0045em'}}>porehenit plictur as et. &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '10.1419em', top: '44.6186em'}}><span
                            className="stl_13 stl_08 stl_34">Calligraphy &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '10.1419em', top: '46.1186em'}}><span
                            className="stl_13 stl_08 stl_35">Writing &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '10.1419em', top: '47.6186em'}}><span
                            className="stl_13 stl_08 stl_22">Blogging &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '10.1419em', top: '49.1186em'}}><span
                            className="stl_13 stl_08 stl_30">Researching &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '49.2081em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '50.0081em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '50.8081em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '21.6997em', top: '49.2081em'}}><span
                            className="stl_13 stl_08 stl_27" style={{wordSpacing: '-0.0062em'}}>Por a duntur aut qui omnienist amene nobitatur Quia si aut &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '21.6997em', top: '50.0081em'}}><span
                            className="stl_13 stl_08 stl_28" style={{wordSpacing: '-0.0046em'}}>molute vel exerior susdaes volupta ectiis et liquas solorep tatur. &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '21.6997em', top: '50.8081em'}}><span
                            className="stl_13 stl_08 stl_29" style={{wordSpacing: '0.0018em'}}>Quiae eaquis apienet rerro evenimus parunt magnihil modiatur. &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '2.9583em', top: '52.1385em'}}><span
                            className="stl_12 stl_08 stl_11"
                            style={{wordSpacing: '0.043em'}}>E D U C A T I O N &nbsp;</span>
                        </div>
                        {
                            values.educations
                                ? values.educations.map((education, index) => (

                                    <>
                                        <div className="stl_01" style={{left: '4.4142em', top: '54.3541em'}}><span
                                            className="stl_24 stl_08 stl_11"
                                            style={{wordSpacing: '0.05em'}}>D E G R E E</span><span
                                            className="stl_24 stl_08 stl_11"
                                            style={{wordSpacing: '0.572em'}}>&nbsp;</span><span
                                            className="stl_24 stl_08 stl_11"
                                            style={{wordSpacing: '0.053em'}}>T I T L E &nbsp;</span>
                                        </div>
                                        <div className="stl_01" style={{left: '4.4143em', top: '55.8728em'}}><span
                                            className="stl_13 stl_08 stl_22"
                                            style={{wordSpacing: '-0.034em'}}>UNIVERSITY NAME &nbsp;</span>
                                        </div>
                                        <div className="stl_01" style={{left: '4.4143em', top: '56.6728em'}}><span
                                            className="stl_13 stl_08 stl_27"
                                            style={{wordSpacing: '-0.023em'}}>Major Subject &nbsp;</span>
                                        </div>
                                        <div className="stl_01" style={{left: '4.4143em', top: '57.4728em'}}><span
                                            className="stl_13 stl_08 stl_22"
                                            style={{wordSpacing: '0em'}}>2030 - 2034 &nbsp;</span>
                                        </div>
                                    </>))
                                : ""}
                        }
                        <div className="stl_01" style={{left: '20.2em', top: '55.8391em'}}><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>J O B</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.572em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>T I T L E</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>H E R E</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>| 2</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>0 3 0</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.6em'}}>- 2</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>0 3 4 &nbsp;</span></div>
                        <div className="stl_01" style={{left: '20.2em', top: '57.7366em'}}><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.0443em'}}>C O M P A N Y</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.566em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>N A M E &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '59.0241em'}}><span
                            className="stl_13 stl_08 stl_16" style={{wordSpacing: '0.0018em'}}>нахуй t acilign atemporpore nes non pratemp &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '59.8241em'}}><span
                            className="stl_13 stl_08 stl_17" style={{wordSpacing: '-0.0051em'}}>orest, tem quost aut vernatius doluptae volum vellorrum inctaque ommolor &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '60.6241em'}}><span
                            className="stl_13 stl_08 stl_25"
                            style={{wordSpacing: '-0.0045em'}}>porehenit plictur as et. &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '4.4142em', top: '60.6641em'}}><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.05em'}}>D E G R E E</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.572em'}}>&nbsp;</span><span
                            className="stl_24 stl_08 stl_11" style={{wordSpacing: '0.053em'}}>T I T L E &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '61.4241em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '62.2241em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '20.1997em', top: '63.0241em'}}><span
                            className="stl_13 stl_08 stl_11">•</span>
                        </div>
                        <div className="stl_01" style={{left: '21.6997em', top: '61.4241em'}}><span
                            className="stl_13 stl_08 stl_27" style={{wordSpacing: '-0.0062em'}}>Por a duntur aut qui omnienist amene nobitatur Quia si aut &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '21.6997em', top: '62.2241em'}}><span
                            className="stl_13 stl_08 stl_28" style={{wordSpacing: '-0.0046em'}}>molute vel exerior susdaes volupta ectiis et liquas solorep tatur. &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '21.6997em', top: '63.0241em'}}><span
                            className="stl_13 stl_08 stl_29" style={{wordSpacing: '0.0018em'}}>Quiae eaquis apienet rerro evenimus parunt magnihil modiatur. &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '4.4143em', top: '62.1828em'}}><span
                            className="stl_13 stl_08 stl_22"
                            style={{wordSpacing: '-0.034em'}}>UNIVERSITY NAME &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '4.4143em', top: '62.9828em'}}><span
                            className="stl_13 stl_08 stl_27"
                            style={{wordSpacing: '-0.023em'}}>Major Subject &nbsp;</span>
                        </div>
                        <div className="stl_01" style={{left: '4.4143em', top: '63.7828em'}}><span
                            className="stl_13 stl_08 stl_22" style={{wordSpacing: '0em'}}>2030 - 2034 &nbsp;</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
};

export default Template1;