import React, {useState} from 'react';
import './style.css';

const Template2 = (props) => {
    const {values} = props;
    const user = values.user ? values.user : {};
    const experiences = values.experiences ? values.experiences : [];
    const educations = values.educations ? values.educations : [];
    const hobbies = values.hobbies ? values.hobbies : [];
    const interests = values.interests ? values.interests : [];
    return (

        <div className={"temp-cont"}>
            <div className="standard__content-wrapper">
                <div className="standard__header">
                    <div className="standard__vertical-line"></div>
                    <div className="standard__name-wrapper">
                        <h1 className="standard__name-wrapper-first-name">{user.firstName}</h1>
                        <h1 className="standard__name-wrapper-last-name">{user.lastName}</h1>
                    </div>
                    <div className="standard__vertical-line-small"></div>
                    <div className="standard__main-container">
                        <div className="standard__left-col">
                            <div className="standard__left-col-contact">
                                <h2>C O N T A C T</h2>
                                <div className="standard__vertical-line-tiny"></div>
                                <div className="standard__left-col-personal-container">
                                    <div>{user.email}</div>
                                    <div>{user.phone}</div>
                                    <div>{user.birthDate}</div>
                                </div>
                            </div>
                            <div className="standard__left-col-skills">
                                <h2>S K I L L S</h2>
                                <ul>
                                    {hobbies.map((key, val) =>
                                        <li>{key}</li>
                                    )}
                                </ul>
                            </div>
                            <div className="standard__left-col-interests">
                                <h2>I N T E R E S T S</h2>

                                <ul>
                                    <ul>
                                        {interests.map((key, val) =>
                                            <li>{key}</li>
                                        )}
                                    </ul>
                                </ul>
                            </div>
                            <div className="standard__left-col-contact">
                                <h2>E D U C A T I O N S</h2>
                                {educations.map((education, val) => <div className={"standard__experience-container"}>
                                    <div className="standard__line-container">
                                        <div className="standard__up-circle">
                                            <div className="standard__up-circle-small"></div>
                                        </div>
                                        <div className="standard__vertical-line standard__center"></div>
                                    </div>
                                    <div className="standard__left-col-edu-container-content">
                                        <h3>{education.degree}</h3>
                                        <div>{education.university}</div>
                                        <div>{education.majorStudy}</div>
                                        <div>{education.yearStart && education.yearStart instanceof Date ? education.yearStart.getFullYear() : ""}
                                            - {education.yearEnd && education.yearEnd instanceof Date ? education.yearEnd.getFullYear() : ""}
                                        </div>
                                    </div>
                                </div>)}
                            </div>

                        </div>
                        <div className="standard__right-col">

                            <h2>P R O F I L E</h2>
                            <div className="standard__right-col-description-wrapper">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor eos mollitia nam
                                    nesciunt rerum!
                                    Aliquam autem deleniti, dignissimos dolor eos id illo iusto molestias necessitatibus
                                    perspiciatis, quae, ut vero voluptatem?
                                </p>
                            </div>
                            <h2>E X P E R I E N C E S </h2>
                            {experiences.map((experience, val) => <div className={"standard__experience-container"}>
                                <div className="standard__line-container">
                                    <div className="standard__up-circle">
                                        <div className="standard__up-circle-small"></div>
                                    </div>
                                    <div className="standard__vertical-line standard__center"></div>
                                </div>
                                <div className="standard__exp-container">
                                    <h3 className="standard__right-col-job-title">{experience.jobTitle}</h3>
                                    <h4 className="standard__right-col-company-name">{experience.company}</h4>
                                    <p>{experience.description}</p>
                                    <div>{experience.workYearStart}
                                        - {experience.workYearEnd}
                                    </div>
                                </div>
                            </div>)}

                        </div>
                    </div>

                </div>
            </div>
        </div>)

};

export default Template2;