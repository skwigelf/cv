import React from 'react';
import '../../../../common/DefaultStyles.css';
import {Field} from "formik";
import {withStyles} from "@material-ui/core";
import styles from "./WorkExperienceStyles";
import LabelField from "../../../../common/LabelField/LabelField";
import LabelTextArea from "../../../../common/LabelField/LabelTextArea";
import MonthPicker from "../../../../../util/MonthPicker";

const WorkExperience = (props) => {
    const {index, classes} = props;
    return (

        <div className={classes.experienceInputWrapper}>
            <div className={classes.multiContainer}>
                <LabelField id="company" type={"text"} name={`experiences[${index}].company`}
                            label={"Название компании"}
                            containerClassName={classes.defaultField}
                            inputClassName={classes.defaultInput}
                            labelClassName={classes.labelStyles}
                            placeholder={"Название компании"}/>
                <LabelField id="jobTitle" type={"text"} name={`experiences[${index}].jobTitle`}
                            label={"Должность"}
                            containerClassName={classes.defaultField}
                            inputClassName={classes.defaultInput}
                            labelClassName={classes.labelStyles}
                            placeholder={"Должность"}/>
            </div>
            <div className={classes.multiContainer}>
                <div className={classes.defaultField}>
                    <label htmlFor={"expStart"} className={classes.labelStyles}>Начало работы</label>
                    <Field id={"expStart"} name={`experiences[${index}].yearStart`} component={MonthPicker}/>
                </div>

                <div className={classes.defaultField}>
                    <label htmlFor={"expEnd"} className={classes.labelStyles}>Окончание работы</label>
                    <Field id={"expEnd"} name={`experiences[${index}].yearEnd`} component={MonthPicker}/>
                </div>
            </div>
            <LabelTextArea id="description" type={"text"} name={`experiences[${index}].description`}
                           label={"Описание работы"}
                           containerClassName={classes.textAreaField}
                           inputClassName={classes.textAreaInput}
                           labelClassName={classes.labelStyles}
                           placeholder={"Описание работы"}/>
        </div>
    );
};

export default withStyles(styles)(WorkExperience);
