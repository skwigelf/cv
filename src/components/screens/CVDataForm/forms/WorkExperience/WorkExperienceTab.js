import React, {useState} from 'react';
import '../../../../common/DefaultStyles.css';
import {Field, FieldArray} from "formik";
import FormTab from "../../section/FormTab";
import WorkExperience from "./WorkExperience";
import {withStyles} from "@material-ui/core";
import styles from "./WorkExperienceStyles";

/*TODO
* При импорте из хх не открывается страница
*
* В опыте работы при добавлении нового опыта копируются данные со старого
*
* */

const WorkExperienceTab = (props) => {
    const {values, classes, isUserAccount} = props;
    const experiences = values ? values.experiences : [];
    const [workTabIndex, setWorkTabIndex] = useState(0);

    return (
        <FormTab title={"Work Experience"} content={
            <div className={isUserAccount ? classes.containerWhite : classes.container}>
                <div className={classes.contentWrapper}>
                    {isUserAccount ? "" : <div>
                        <h2 className={classes.experienceHeader}>Опыт работы</h2>
                    </div>}
                    <FieldArray
                        name={"workExperience"}
                        render={array => (
                            <div>
                                {values ? experiences.map((experience, index) => (
                                    <span
                                        className={index === workTabIndex
                                            ? classes.workTab + ' ' + classes.activeWorkTab
                                            : classes.workTab}
                                        onClick={() => {
                                            setWorkTabIndex(index)
                                        }}
                                    >
                                        Work Experience {index + 1} {experience.company ? '(' + experience.company + ')' : ''}
                                    </span>
                                )) : ""}

                                <div className={"container education-form"}>
                                    <div className={"row"}>
                                        <div className={"education-wrapper col-md-12"}>
                                            <WorkExperience index={workTabIndex}/>
                                            <span onClick={() => {
                                                setWorkTabIndex(workTabIndex + 1);
                                                experiences.push({});
                                            }
                                            }>+Добавить еще</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}/>
                </div>
            </div>
        }/>
    );
};

export default withStyles(styles)(WorkExperienceTab);