const styles = (theme) => ({
    container: {
        backgroundColor: '#e0e0e0',
        width: '100%',
        borderRadius: '10px',
    },
    contentWrapper: {
        paddingTop: '20px',
        paddingBottom: '20px',
        boxSizing: 'border-box',
        paddingLeft: '50px',
        paddingRight: '50px',
        width: '100%',
        margin: 'auto'
    },
    photoContainer: {
        marginBottom: '10px',
    },
    experienceHeader: {
        textAlign: 'center',
    },
    multiContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    defaultField: {
        width: '47%',
    },
    labelStyles: {
        display: 'block',
        paddingTop: '5px',
        paddingBottom: '5px',
    },
    defaultInput: {
        margin: 'auto 0 17px',
        display: 'block',
        width: '100%',
        borderWidth: '2px',
        borderColor: '#d4dae7',
        textDecoration: 'none',
        padding: '10px 0 10px 16px',
        borderRadius: '15px',
        borderStyle: 'solid',
        boxSizing: 'border-box',
        height: '42px',

    },
    textAreaInput: {
        margin: 'auto 0 17px',
        display: 'block',
        width: '100%',
        borderWidth: '2px',
        borderColor: '#d4dae7',
        textDecoration: 'none',
        padding: '10px 0 10px 16px',
        borderRadius: '15px',
        borderStyle: 'solid',
        boxSizing: 'border-box',
        height: '120px',
    },
    birthPhoneContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    textAreaField: {
        width: '100%',
    },
    activeWorkTab: {
        color: '#883eb0',
    },
    workTab: {
        paddingRight: '10px',
        cursor: 'pointer',
    },
    containerWhite: {
        backgroundColor: '#ffffff',
        width: '100%',
        borderRadius: '10px',
    },
    '@media (max-width: 768px)': {
        container: {
            backgroundColor: '#ffffff',
            width: '100%',
            borderRadius: '10px',
        },
        defaultField: {
            width: '100%',
        },
        multiContainer: {
            display: 'block',
            justifyContent: 'space-between',
        },
        birthPhoneContainer: {
            display: 'block',
            justifyContent: 'space-between',
        },
    }
});

export default styles;