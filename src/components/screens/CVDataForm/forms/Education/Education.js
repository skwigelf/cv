import React from 'react';
import '../../../../common/DefaultStyles.css';
import {Field} from "formik";
import {withStyles} from "@material-ui/core";
import styles from "./EducationStyles";
import LabelField from "../../../../common/LabelField/LabelField";
import YearPicker from "../../../../../util/YearPicker";

const Education = (props) => {
    const {index, type, classes} = props;
    console.log(props);
    return (


        <div className={"education-input-wrapper"}>
            <div className={''}>
                <label htmlFor={"type"} className={classes.labelStyles}>Тип</label>
                <Field id="type" name={`educations[${index}]type`} component="select" className={classes.defaultInput}>
                    <option value="HIGH">Высшее</option>
                    <option value="SECONDARY">Среднее</option>
                    <option value="SPECIAL">Средне-специальное</option>
                </Field>
            </div>
            <div className={classes.multiContainer}>

                {type && type === "HIGH"
                    ?
                    <LabelField id="degree" type={"text"} name={`educations[${index}].degree`}
                                label={"Ученая степень"}
                                className={"input-user-default"}
                                containerClassName={classes.defaultField}
                                inputClassName={classes.defaultInput}
                                labelClassName={classes.labelStyles}
                                placeholder={"Ученая степень"}/>
                    : ""}
                <LabelField id="university" type={"text"} name={`educations[${index}].university`}
                            label={"Наименование заведения"}
                            className={"input-user-default"}
                            containerClassName={classes.defaultField}
                            inputClassName={classes.defaultInput}
                            labelClassName={classes.labelStyles}
                            placeholder={"Наименование заведения"}/>
            </div>
            <div className={classes.multiContainer}>
                {type && type !== "SECONDARY"
                    ? <LabelField id="faculty" type={"text"} name={`educations[${index}].faculty`}
                                  label={"Факультет"}
                                  containerClassName={classes.defaultField}
                                  className={"input-user-default"}
                                  inputClassName={classes.defaultInput}
                                  labelClassName={classes.labelStyles}
                                  placeholder={"Факультет"}/>
                    : ""}
                <LabelField id="majorStudy" type={"text"} name={`educations[${index}].majorStudy`}
                            label={"Специальность"}
                            containerClassName={classes.defaultField}
                            className={"input-user-default"}
                            inputClassName={classes.defaultInput}
                            labelClassName={classes.labelStyles}
                            placeholder={"Специальность"}/>
            </div>
            <div className={classes.multiContainer}>
                <div className={classes.defaultField}>
                    <label htmlFor={"expEnd"} className={classes.labelStyles}>Год Начала обучения</label>
                    <Field id={"eduStart"} name={`educations[${index}].yearStart`} component={YearPicker}/>
                </div>
                <div className={classes.defaultField}>
                    <label htmlFor={"expEnd"} className={classes.labelStyles}>Год окончания обучения</label>
                    <Field id={"eduEnd"}  name={`educations[${index}].yearEnd`} component={YearPicker}/>
                </div>

            </div>
        </div>


    );
};

export default withStyles(styles)(Education);
