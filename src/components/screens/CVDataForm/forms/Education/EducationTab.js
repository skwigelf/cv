import React, {useState} from 'react';
import '../../../../common/DefaultStyles.css';
import {FieldArray} from "formik";
import FormTab from "../../section/FormTab";
import Education from "./Education";
import styles from "./EducationStyles";
import {withStyles} from "@material-ui/core";

const EducationTab = (props) => {
    const {isUserAccount, values, classes} = props;
    const educations = values && values.educations && values.educations.length > 0 ? values.educations : [{type: 'HIGH'}];
    const [eduTabIndex, setEduTabIndex] = useState(0);
    console.log(educations);
    console.log(values);
    return (
        <FormTab title={"Образование"} content={
            <div className={isUserAccount ? classes.containerWhite : classes.container}>
                <div className={classes.contentWrapper}>
                    {isUserAccount ? "" : <div>
                        <h2 className={classes.educationHeader}>Образование</h2>
                    </div>}
                    <FieldArray
                        name={"educations"}>
                        {({insert, remove, push}) => (
                            <div>
                                <div className={classes.universitiesContainer}>


                                    {values ? educations.map((education, index) => (
                                        <div className={classes.eduMenu}>
                                        <span
                                            className={index === eduTabIndex
                                                ? classes.eduTab + ' ' + classes.activeEduTab
                                                : classes.eduTab}
                                            onClick={() => {
                                                setEduTabIndex(index)
                                            }}
                                        >
                                         Образование {index + 1} {education.university ? '(' + education.university + ')' : ''}
                                        </span>
                                            <span onClick={() => {
                                                //setEduTabIndex(index);
                                                remove(index);
                                            }}>
                                                / x
                                            </span>
                                        </div>

                                    )) : ""}
                                </div>
                                <div className={"container education-form"}>
                                    <div className={"education-wrapper col-md-12"}>
                                        <Education index={eduTabIndex} type={educations[eduTabIndex] ? educations[eduTabIndex].type : ""}/>
                                        <span onClick={() => {
                                            setEduTabIndex(eduTabIndex + 1);
                                            push({
                                                type: 'HIGH',
                                                degree: '',
                                                university: '',
                                                faculty: '',
                                                majorStudy: '',
                                                yearStarted: '',
                                                yearEnded: ''
                                            });

                                        }
                                        }>+ Добавить еще</span>
                                    </div>
                                </div>
                            </div>
                        )}
                    </FieldArray>
                </div>
            </div>
        }/>
    );
};

export default withStyles(styles)(EducationTab);