import React, {useState} from 'react';
import '../../../../common/DefaultStyles.css';
import {withStyles} from "@material-ui/core";
import LabelField from "../../../../common/LabelField/LabelField";
import styles from "./HobbieInterestsStyles";
import {FieldArray} from "formik";

const HobbiesInterests = (props) => {

    const [hobbieIndex, setHobbieIndex] = useState(0);
    const [interestIndex, setInterestIndex] = useState(0);
    const [value, setValue] = useState("");
    const {values, classes, isUserAccount} = props;
    console.log(value);
    console.log(hobbieIndex);

    return (
        <div className={isUserAccount ? classes.containerWhite : classes.container}>
            <div className={classes.contentWrapper}>
                {isUserAccount ? "" : <div>
                    <h2 className={classes.contactHeader}>Хобби и навыки</h2>
                </div>}

                <div className={"contact-input-wrapper"}>
                    <div className={classes.hobbiesSkillsWrapper}>
                        <FieldArray
                            name={"hobbies"}>
                            {({insert, remove, push}) => (

                                <div className={classes.hobbiesMainContainer}>
                                    <div className={classes.fieldWrapper}>

                                        <LabelField id="hobbies"
                                                    type={"text"}
                                                    name={`hobbies[${hobbieIndex}]`}
                                                    placeholder={"хобби"} label={"Хобби"}
                                                    containerClassName={classes.defaultField}
                                                    inputClassName={classes.defaultInput}
                                                    labelClassName={classes.labelStyles}
                                        />
                                        <div>
                                            <span className={classes.addButton} onClick={() => {
                                                setHobbieIndex(hobbieIndex + 1);
                                                push('Новые хобби');
                                                console.log(values);
                                            }}>
                                                Добавить хобби
                                            </span>
                                        </div>
                                    </div>
                                    <div className={classes.hobbiesContainer}>
                                        {values.hobbies ? values.hobbies.map((key, index, val) =>
                                            <div className={classes.valueWrapper} onClick={() => setHobbieIndex(index)}>
                                            <span className={hobbieIndex === index ? classes.active : ""}>
                                                {key}
                                            </span>
                                                <span onClick={() => {
                                                    //values.hobbies.splice(index, 1);
                                                    console.log(index);
                                                    console.log(hobbieIndex);
                                                    let newHobbieIndex = 0;
                                                    if (index < hobbieIndex) {
                                                        newHobbieIndex = index;
                                                    } else if (index > hobbieIndex) {
                                                        newHobbieIndex = hobbieIndex;
                                                    }
                                                    setHobbieIndex(newHobbieIndex);
                                                    remove(index);

                                                }} className={classes.deleteElement}>\ x  </span>

                                            </div>
                                        ) : ""}
                                    </div>
                                </div>
                            )}
                        </FieldArray>


                        <FieldArray
                            name={"interests"}>
                            {({insert, remove, push}) => (
                                <div className={classes.skillsMainContainer}>
                                    <div className={classes.fieldWrapper}>
                                        <LabelField id="interests" type={"text"}
                                                    name={`interests[${interestIndex}]`}
                                                    containerClassName={classes.defaultField}
                                                    inputClassName={classes.defaultInput}
                                                    labelClassName={classes.labelStyles}
                                                    placeholder={"Навык"} label={"Навыки"}/>

                                        <div>
                                        <span className={classes.addButton} onClick={() => {
                                            setInterestIndex(interestIndex + 1);
                                            push('Новый навык');
                                            console.log(values);
                                        }}>Добавить навык
                                        </span>

                                        </div>
                                    </div>
                                    <div className={classes.interestsContainer}>
                                        {values.interests ? values.interests.map((key, index, val) =>
                                            <div className={classes.valueWrapper} onClick={() => setInterestIndex(index)}>
                                                <span>
                                                    {key}
                                                </span>
                                                <span onClick={() => {
                                                    //values.hobbies.splice(index, 1);
                                                    console.log(index);
                                                    console.log(interestIndex);
                                                    let newInterestIndex = 0;
                                                    if (index < hobbieIndex) {
                                                        newInterestIndex = index;
                                                    } else if (index > interestIndex) {
                                                        newInterestIndex = interestIndex;
                                                    }
                                                    setHobbieIndex(newInterestIndex);
                                                    remove(index);

                                                }} className={classes.deleteElement}>\ x</span>
                                            </div>
                                        ) : ""}
                                    </div>
                                </div>)}
                        </FieldArray>


                    </div>

                </div>
            </div>
        </div>
    );
};

export default withStyles(styles)(HobbiesInterests);
