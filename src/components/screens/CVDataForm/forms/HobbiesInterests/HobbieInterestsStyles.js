const styles = (theme) => ({
    container: {
        backgroundColor: '#e0e0e0',
        width: '100%',
        borderRadius: '10px',
    },
    hobbiesSkillsWrapper: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    hobbiesMainContainer: {
        width: '45%',
    },
    skillsMainContainer: {
        width: '45%',
    },
    containerWhite: {
        backgroundColor: '#ffffff',
        width: '100%',
        borderRadius: '10px',
    },
    contentWrapper: {
        paddingTop: '20px',
        paddingBottom: '20px',
        boxSizing: 'border-box',
        paddingLeft: '50px',
        paddingRight: '50px',
        width: '100%',
        margin: 'auto'
    },
    photoContainer: {
        marginBottom: '10px',
    },
    contactHeader: {
      textAlign: 'center',
    },
    nameContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    defaultField: {
        width: '100%',
    },
    labelStyles: {
        display: 'block',
        paddingTop: '5px',
        paddingBottom: '5px',
    },
    defaultInput: {
        margin: 'auto 0 17px',
        display: 'block',
        width: '100%',
        borderWidth: '2px',
        borderColor: '#d4dae7',
        textDecoration: 'none',
        padding: '10px 0 10px 16px',
        borderRadius: '15px',
        borderStyle: 'solid',
        boxSizing: 'border-box',
        height: '42px',

    },
    birthPhoneContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    valuesContainer: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    hobbiesContainer: {

    },
    interestsContainer: {

    },
    fieldWrapper: {
    },
    deleteElement: {
        color: '#883eb0'
    },
    valueWrapper: {
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: '5px',
        marginBottom: '5px',
        cursor: 'pointer'
    },
    active: {
        color: '#883eb0'
    },
    addButton: {
        color: '#883eb0',
        cursor: 'pointer'
    }
});

export default styles;