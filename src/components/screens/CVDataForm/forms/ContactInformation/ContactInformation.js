import React from 'react';
import '../../../../common/DefaultStyles.css';
import {Field} from "formik";
import {withStyles} from "@material-ui/core";
import styles from "./ContactInformationStyles";
import LabelField from "../../../../common/LabelField/LabelField";
import MaskedInput from "react-text-mask";

const phoneNumberMask = [
    /\d/,
    "(",
    /[1-9]/,
    /\d/,
    /\d/,
    ")",
    " ",
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/
];

const ContactInformation = (props) => {

    const {classes, isUserAccount} = props;
    return (
        <div className={isUserAccount ? classes.containerWhite : classes.container}>
            <div className={classes.contentWrapper}>
                {isUserAccount ? "" : <div>
                    <h2 className={classes.contactHeader}>О себе. Общее</h2>
                </div>}
                <div className={"contact-input-wrapper"}>
                    <div className={classes.photoContainer}>
                        <h4>Фото</h4>
                        <span>Добавить фото</span>
                    </div>
                    <div className={classes.nameContainer}>
                        <LabelField id="firstName"
                                    className={"input-user-default"}
                                    type={"text"}
                                    name={`user.firstName`}
                                    placeholder={"FirstName"} label={"Имя"}
                                    containerClassName={classes.defaultField}
                                    inputClassName={classes.defaultInput}
                                    labelClassName={classes.labelStyles}/>
                        <LabelField id="lastName" type={"text"} name={`user.lastName`}
                                    className={"input-user-default"}
                                    containerClassName={classes.defaultField}
                                    inputClassName={classes.defaultInput}
                                    labelClassName={classes.labelStyles}
                                    placeholder={"Last name"} label={"Фамилия"}/>
                    </div>

                    <div className={classes.birthPhoneContainer}>
                        <LabelField id="birthDate" type={"date"} name={`user.birthDate`}
                                    className={"input-user-default"}
                                    placeholder={"Birth date"} label={"Дата рождения"}
                                    containerClassName={classes.defaultField}
                                    inputClassName={classes.defaultInput}
                                    labelClassName={classes.labelStyles}/>
                        <div className={classes.defaultField}>
                            <label htmlFor={"phone"} className={classes.labelStyles}>Телефон</label>
                            <Field id={"phone"} label={"Телефон"} name={`user.phone`} className={"input-user-default"}>
                                {({field}) => <MaskedInput
                                    {...field}
                                    className={classes.defaultInput}
                                    mask={phoneNumberMask}
                                    id="phone"
                                    placeholder="Enter your phone number"
                                    type="text"
                                    ></MaskedInput>}
                            </Field>
                        </div>

                    </div>
                    <LabelField id="email" type={"email"} name={`user.email`} className={"input-user-default"}
                                placeholder={"Email"} label={"Email"} labelClassName={classes.labelStyles}/>
                </div>
            </div>
        </div>
    );
};

export default withStyles(styles)(ContactInformation);
