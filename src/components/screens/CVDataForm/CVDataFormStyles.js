const styles = (theme) => ({
    chooseTemplateContainer: {
        width: '1250px',
        margin: 'auto'
    },
    chooseTemplateItemContainer: {
        display: 'flex',
        alignContent: 'flex-start',
        justifyContent: 'space-between'
    },
});

export default styles;