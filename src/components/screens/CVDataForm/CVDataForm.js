import React from 'react';
import '../../common/DefaultStyles.css';
import FormSliderContainer from "../FormSliderContainer/FormSliderContainer";
import ContactInformation from "./forms/ContactInformation/ContactInformation";
import EducationTab from "./forms/Education/EducationTab";
import WorkExperienceTab from "./forms/WorkExperience/WorkExperienceTab";
import HobbiesInterests from "./forms/HobbiesInterests/HobbiesInterests";
const CVDataForm = (props) => {

    const {cvId, data, setStatus, setData} = props;

    const formTypes = [ContactInformation, EducationTab, WorkExperienceTab, HobbiesInterests];
    return (
        <div className={"screen-container"}>
            <div className={"choose-template-title-container"}>
                <h1>Шаг 2. Расскажи о себе</h1>
            </div>
            <FormSliderContainer cvId={cvId} formTypes={formTypes} data={data} setData={setData} setStatus={setStatus}/>
        </div>
    );
};

export default CVDataForm;
